GROUP 45: C Compiler in Python
Rushab Munot (14405)
Devendra Meena (14214)

____________________________________________
1>First, cd to the folder Python/src/ 
cd Python/src/

2>To run the parser on a specific file:
python parser.py <file_loation>

example: python parser.py ../sample_programs/Fibonacci.c

3>For operator type checking run this program as 
python parser.py <file_location> -AST 1

4>To execute the assembly code 
./x86.sh

____________________________________________
cd Python/src 
python parser.py ../sample_programs/Fibonacci.c
./x86.sh
____________________________________________
The sample codes are kept in the directory: sample_programs/

The assembly output is generated in the file: bin/output.asm

The three address code is generated in the file: bin/TACFile.csv

__________________________________________________________
Test cases that are working:
1) Ackermann
2) Bubblesort
3) many_param ( function with multi parameters )
4) operatorsTypeCheck ( to check the type of operators ) 
5) binary_search
6) Fibonacci
7) odd_even ( mutual recursion )
8) unary_minus ( unary operators in expressions )
___________________________________________________________
____________________________________________
References:
Grammar - lex and yacc files : 
[https://github.com/eliben/pycparser](https://github.com/eliben/pycparser)
