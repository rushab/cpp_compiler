from copy import deepcopy
from collections import deque
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

typeSizes = {
        'int': 4,
        'float': 4,
        'char': 1,
        'long': 4,
        'short': 2,
        'pointer': 4
        }

def returnWidth(idtype):
    return typeSizes[idtype]

def type(i):
    try:
        int(i)
        return 'int'
    except:
        try:
            float(i)
            return 'float'
        except:
            return 'id'

def checkFunc(f):
    if len(f) >= 2 and isinstance(f[1], tuple) and f[0] == None and isinstance(f[1][1], list):
        return True
    else:
        return False

def checkStruct(s):
    if len(s) == 2 and s[1] == 'struct':
        return True
    return False

class Node:
    def __init__(self, nodeName, parent = None):
        self.name = nodeName
        self.parent = parent
        self.children = []
        self.data = None

    def parent(self):
        return self.parent

    def children(self):
        return self.children

    def addChild(self, child):
        self.children.append(child)
        child.parent = self

class Tree:
    def __init__(self):
        self.root = Node('global')
        self.current = self.root

    def createEmptyNode(self, nodeName = None):
        newNode = Node(nodeName, parent = self.current )
        self.current.addChild(newNode)
        self.current = newNode

    def addData(self, data):
        self.current.data = data

    def parent(self):
        return self.current.parent

    def changeCurrent(self, newCurrent):
        self.current = newCurrent

    def printTree(self, node, numTabs = 0):
        curNode = deepcopy(node)

        print('SCOPE: ' + str(curNode.name))
        self.printNode(curNode, numTabs)
        for i in curNode.children:
            self.printTree(i, numTabs + 1)
        
    def printNode(self, node, numTabs):
        if not node.data:
            return
        for i in node.data:
            tabStr = ""
            for j in range(0, numTabs):
                tabStr += "\t"
            print(tabStr + i + "\t: " + str(node.data[i]))

    def changeNodeName(self, node, name):
        node.name = name

    def searchScope(self, searchName, isStruct = False):
        root = self.root

        if isStruct:
            return root.data[searchName][0]

        queue = deque()
        queue.append(root)
        while len(queue) > 0:
            curNode = queue.popleft()
            if curNode.name == searchName:
                return curNode.data
            for i in curNode.children:
                queue.append(i)
        return -1


class Stack(object):
    def __init__(self):
        self.stack = []
    
    def push(self, x):
        self.stack.append(x)

    def pop(self):
        if len(self.stack) != 0:
            tmp = self.stack[len(self.stack) - 1]
            del self.stack[len(self.stack) - 1]
            return tmp 
        else:
            print bcolors.WARNING +  "Warning: " + bcolors.ENDC + "Trying to pop from empty stack"
            return None

    def pushpop(self, x):
        self.pop()
        self.push(x)
     
    def size(self):
        return len(self.stack)

    def addtoTop(self, x, y):
        self.stack[len(self.stack) - 1][y] = x

    def changeValue(self, x, y):
        for i in range(len(self.stack) - 1, -1 , -1):
            try:
                self.stack[ i ][y][0] = x
                return
            except:
                pass 

    def addBelowTop(self, x, y):
        self.stack[len(self.stack) - 2][y] = x

    def searchTop(self, x):
        stacksize = len(self.stack)  - 1

        if x in self.stack[ stacksize ]:
            return self.stack[ stacksize ][x]
        else:
            return False

    def searchBelowTop(self, x):
        stacksize = len(self.stack) - 2
        if x in self.stack[ stacksize ]:
            return self.stack[ stacksize ][x]
        else:
            return False

    def search(self, x):
        for j in range( len(self.stack) - 1, -1, -1):
            i = self.stack[j]
            if isinstance(i, dict) and x in i:
                return i[x]
        return False

    def top(self):
        return self.stack[len(self.stack) - 1]

    def empty(self):
        return len(self.stack) == 0
