for f in ../sample_programs/*;
do
    tput setaf 1; echo $f
    tput setaf 7;
    python parser.py $f -AST $1
done
