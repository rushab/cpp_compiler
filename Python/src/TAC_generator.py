from copy import deepcopy
count = 0

class Attributes:
    def __init__(self, node = None):
        self.node = node
        self.nextList = []
        self.falseList = []
        self.trueList = []
        self.breakList = []
        self.continueList = []
        self.next = None
        self.result = None

class Marker:
    def __init__(self, node = None):
        self.node = node
        self.quad = None
        self.nextList = []
        self.falseList = []

class Expression:
    def __init__(self, node = None):
        self.value = None
        self.trueList = []
        self.falseList = []

def genTmpVar():
    global count 
    count += 1
    newVarName = "tmp" + str(count)
    return newVarName

def genQuad(op1, op2, op, res = -1):
    if res == -1:
        return [genTmpVar(), op1, op2, op]
    else:
        return [res, op1, op2, op]

def makelist():
    return []

def backpatch(quadlist, valList, target):
    for i in valList:
        quadlist[i][2] = target

def concat(list1, list2, list3 = -1):
    if list3 != -1:
        list3 = deepcopy(list1)
        list3.extend(deepcopy(list2))
        return list3
    else:
        list1.extend(list2)

