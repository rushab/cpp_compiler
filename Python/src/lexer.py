from c_lexer import CLexer

def error_func(errMessage, line, column):
    print("Error encountered")

def on_lbrace():
    pass

def on_rbrace():
    pass

def type_lookup_func():
    pass

def lex(filename):
    codeFile = open(filename)
    codeText = codeFile.read()
    codeFile.close()

    lexer = CLexer(error_func, on_lbrace, on_rbrace, type_lookup_func)
    lexer.build()
    # print(codeText)
    lexer.input(codeText)
    return codeText, lexer

