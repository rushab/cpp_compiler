# CParser class: Parser and AST builder for the C language
#
# Eli Bendersky [http://eli.thegreenplace.net]
# License: BSD
########################################################
import re
from TAC_generator import *
from tree_generator import *
from ply import yacc
# from . import c_ast
# import c_ast
from x86_generator import *
import csv

from c_lexer import CLexer
from plyparser import PLYParser, Coord, ParseError, parameterized, template
# from ast_transforms import fix_switch_cases

from functions import *
from copy import deepcopy
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

@template
class CParser(PLYParser):
    def __init__(
            self,
            dotFile,
            symbolTableFile,
            lex_optimize=True,
            lexer=CLexer,
            lextab='pycparser.lextab',
            yacc_optimize=True,
            yacctab='pycparser.yacctab',
            yacc_debug=False,
            taboutputdir=''):
        """ Create a new CParser.

            Some arguments for controlling the debug/optimization
            level of the parser are provided. The defaults are
            tuned for release/performance mode.
            The simple rules for using them are:
            *) When tweaking CParser/CLexer, set these to False
            *) When releasing a stable parser, set to True

            lex_optimize:
                Set to False when you're modifying the lexer.
                Otherwise, changes in the lexer won't be used, if
                some lextab.py file exists.
                When releasing with a stable lexer, set to True
                to save the re-generation of the lexer table on
                each run.

            lexer:
                Set this parameter to define the lexer to use if
                you're not using the default CLexer.

            lextab:
                Points to the lex table that's used for optimized
                mode. Only if you're modifying the lexer and want
                some tests to avoid re-generating the table, make
                this point to a local lex table file (that's been
                earlier generated with lex_optimize=True)

            yacc_optimize:
                Set to False when you're modifying the parser.
                Otherwise, changes in the parser won't be used, if
                some parsetab.py file exists.
                When releasing with a stable parser, set to True
                to save the re-generation of the parser table on
                each run.

            yacctab:
                Points to the yacc table that's used for optimized
                mode. Only if you're modifying the parser, make
                this point to a local yacc table file

            yacc_debug:
                Generate a parser.out file that explains how yacc
                built the parsing table from the grammar.

            taboutputdir:
                Set this parameter to control the location of generated
                lextab and yacctab files.
        """
        self.dotFile = dotFile
        self.stf = csv.writer(symbolTableFile)
        self.normalstf = symbolTableFile
        self.function_definition_flag = False
        self.for_flag = 0
        self.numScopes = 0

        self.scopeStack = Stack()
        self.scopeNames = Stack()
        self.scopeStack.push(dict()) # push global scope
        self.scopeTree = Tree()
        self.evalStack  = Stack()
        self.evalStack.push('$')
        self.no_of_arg = Stack() # Just a temporarily used variable to store number of arguments in a function call
        self.astStack = Stack() # Stack for computation related to AST
        self.quadList = [] #Quadraple list
        self.funcParamList = {}

        self.clex = lexer(
            error_func=self._lex_error_func,
            on_lbrace_func=self._lex_on_lbrace_func,
            on_rbrace_func=self._lex_on_rbrace_func,
            type_lookup_func=self._lex_type_lookup_func)

        self.clex.build(
            optimize=lex_optimize,
            lextab=lextab,
            outputdir=taboutputdir)
        self.tokens = self.clex.tokens

        rules_with_opt = [
            'abstract_declarator',
            'assignment_expression',
            'declaration_list',
            'declaration_specifiers_no_type',
            'designation',
            'expression',
            'identifier_list',
            'init_declarator_list',
            'id_init_declarator_list',
            'initializer_list',
            'parameter_type_list',
            'block_item_list',
            'type_qualifier_list',
            'struct_declarator_list'
        ]

        for rule in rules_with_opt:
            self._create_opt_rule(rule)

        self.cparser = yacc.yacc(
            module=self,
            start='translation_unit_or_empty',
            debug=yacc_debug,
            optimize=yacc_optimize,
            tabmodule=yacctab,
            outputdir=taboutputdir)

    ###############################START DEFINING FUNCTIONS###########################################################
    def _raise_error(self, lineno, errorMsg, errorType = 'Type', var = None):
        errorMsg = bcolors.WARNING + "Error at line " + str(lineno) + ": " + bcolors.FAIL + errorMsg + bcolors.ENDC
        print(errorMsg)
        raise SyntaxError

    def _warning(self, lineno, warningMsg):
        print(bcolors.WARNING + "Warning at line " + str(lineno) + " : " + warningMsg + bcolors.ENDC)

    def _extract_id_list_to_scope(self, lineno):
        tmp = self.evalStack.pop()

        if tmp == 'FORWARD_DECLARATION':
            argType = []
            #Ignoring arrays and pointers for now
            tmp = self.evalStack.pop()
            while tmp != '$':
                if tmp == ',':
                    tmp =self.evalStack.pop()
                if tmp != '$':
                    argType.append(tmp)
                tmp =self.evalStack.pop()

            var = self.evalStack.pop()
            if self.scopeStack.searchTop(var):
                errorMsg = "Redefinition of variable " + str(var)
                self._raise_error(self.clex.lexer.lineno, errorMsg, errorType = 'redefinition', var = var )
            #Ignoring arrays and pointers for now
            retType = self.evalStack.pop()
            argType.reverse()
            self.scopeStack.addtoTop([None, (retType, argType, "function_undeclared")], var)
            return

        ##################################
        id_list = []
        while tmp != "$" and tmp != '$Struct_declarations':
            id_list.append(tmp)
            tmp = self.evalStack.pop()

        if tmp == '$Struct_declarations':
            id_list[ len(id_list) - 1 ] = ('struct', id_list[ len(id_list) - 1 ])

        id_type = id_list[len(id_list)-1]
        tmp_top = self.scopeStack.top()

        i = len(id_list) - 2

        while(i >= 0):
            if isinstance(id_list[i], str):
                tmp = self.scopeStack.searchTop(id_list[i])
                if tmp != False and tmp[1] != None:
                    self._raise_error( lineno, "Redefinition_1 of " + str(id_list[i]))

            i_tmp = i
            id_list_tmp = deepcopy(id_list)
            id_type_tmp = id_type
            try:
                if id_list[i] == ',':
                    i = i - 2
                    ##################################################
                    tmp_pointer_count = 0
                    tmpcheck = 0
                    while id_list[i] == ("*", "pointer"):
                        tmp_pointer_count += 1
                        i -= 1

                    if isinstance(id_list[ i ], tuple) and id_list[i][0] == "array":
                        id_type = (id_type, "array", id_list[i][1])
                        i -= 1

                    if tmp_pointer_count >= 1:
                        tmpcheck = 1
                        id_type = ("pointer", id_type)

                    for j in range(0, tmp_pointer_count - 1):
                        id_type = ("pointer", id_type)
                    ##################################################

                    tmp = self.scopeStack.searchTop(id_list[i])
                    if tmp != False and tmp[1].node != None:
                        self._raise_error( lineno, "Redefinition_2 of " + str(id_list[i]))
                    else:
                        tmp_top[id_list[i]][1] = id_list[i + 1]
                else:
                    if id_list[i] == ("*", "pointer"):
                        i = i - 1
                        ##################################################
                        tmp_pointer_count = 0
                        while id_list[i] == ("*", "pointer"):
                            tmp_pointer_count += 1
                            i -= 1

                        if isinstance(id_list[ i ], tuple) and id_list[i][0] == "array":
                            id_type = (id_type, "array", id_list[i][1])
                            i -= 1

                        if tmp_pointer_count >= 1:
                            id_type = ("pointer", id_type)

                        for j in range(0, tmp_pointer_count - 1):
                            id_type = ("pointer", id_type)

                        ##################################################
                        tmp_top[id_list[i]][1] = ( "pointer", id_type)
                    else:
                        tmp_top[id_list[i]][1] = id_type
                id_type = id_type_tmp
            except:
                id_type = id_type_tmp
                i = i_tmp
                id_list = deepcopy(id_list_tmp)
                if id_list[i] == ',':
                    i = i - 2
                    ##################################################
                    tmp_pointer_count = 0
                    tmpcheck = 0
                    while id_list[i] == ("*", "pointer"):
                        tmp_pointer_count += 1
                        i -= 1

                    if isinstance(id_list[i], tuple) and id_list[i][0] == "array":
                        id_type = (id_type, "array", id_list[i][1])
                        i -= 1

                    if tmp_pointer_count >= 1:
                        tmpcheck = 1
                        id_type = ("pointer", id_type)

                    for j in range(0, tmp_pointer_count - 1):
                        id_type = ("pointer", id_type)
                    ##################################################
                    # if id_list[i] == ("*", "pointer"):
                        # id_list[i] = id_type
                        # i = i-1
                    if tmpcheck == 1:
                        id_list[i + 1] = id_type
                    tmp = self.scopeStack.searchTop(id_list[i])
                    if tmp != False and tmp[1].node != None:
                        self._raise_error( lineno, "Redefinition_2 of " + str(id_list[i]))
                    else:
                        # tmp_top[id_list[i]] = [None, id_list[i + 1]]
                        tmp_top[id_list[i]] = [None, id_list[i + 1]]
                else:
                    if id_list[i] == ("*", "pointer"):
                        i = i - 1
                        ##################################################
                        tmp_pointer_count = 0
                        while id_list[i] == ("*", "pointer"):
                            tmp_pointer_count += 1
                            i -= 1

                        if isinstance(id_list[ i ], tuple) and id_list[i][0] == "array":
                            id_type = (id_type, "array", id_list[i][1])
                            i -= 1

                        if tmp_pointer_count >= 1:
                            id_type = ("pointer", id_type)

                        for j in range(0, tmp_pointer_count - 1):
                            id_type = ("pointer", id_type)

                        ##################################################
                        tmp_top[id_list[i]] = [ None, ("pointer", id_type)]
                    else:
                        if isinstance(id_list[ i ], tuple) and id_list[i][0] == "array":
                            id_type = (id_type, "array", id_list[i][1])
                            i -= 1

                        tmp_top[id_list[i]] = [ None, id_type]

                id_type = id_type_tmp
            i = i- 1

    def astForBinaryExpr( self, p):
        t1 = self.evalStack.pop()
        t2 = self.evalStack.pop()
        
        #######optimize for constants##################33
        # if self.for_flag == 0:
            # type1 = type(t1)
            # type2 = type(t2)

            
            # if p[2] in ['+', '-', '*', '/', '%', '<<', '>>']:
                # tmp = self.scopeStack.search(t1)
                # if type1 == 'id':
                    # tmp = self.scopeStack.search(t1)
                    # if type(tmp[0]) == 'int' or 'float':
                        # tmp1 = tmp[0]
                        # type1 = type(tmp[0])
                # else:
                    # tmp1 = t1
                # if type2 == 'id':
                    # tmp = self.scopeStack.search(t2)
                    # if type(tmp[0]) == 'int' or 'float':
                        # tmp2 = tmp[0]
                        # type2 = type(tmp[0])
                # else:
                    # tmp2 = t2

                # if type1 in ['int', 'float', 'char'] and type2 in ['int', 'float', 'char']:
                    # t1 = tmp1
                    # t2 = tmp2
                    # self.astStack.pop()
                    # self.astStack.pop()
                    # val =  eval(t2 + p[2] + t1)

                    # if isinstance(val, bool):
                        # val = int(val)
                    # val = str(val)

                    # self.evalStack.push(val)

                    # if type(val) in ['int', 'char']:
                        # self.astStack.push((val, 'int', 'constant'))
                        # p[0].node = genNodes([ p[2] + 'i', p[1].node, p[3].node], self.dotFile)
                    # elif type(val) == 'float':
                        # self.astStack.push((val, 'float', 'constant'))
                        # p[0].node = genNodes([ p[2] + 'f', p[1].node, p[3].node], self.dotFile)
                    # return val


        ################################################
        

        #generate quadruples
        tmpquad = genQuad(t2, t1, p[2])
        self.quadList.append(tmpquad)
        self.evalStack.push(tmpquad[0])
        tmpquad_expr = tmpquad[1] + tmpquad[3] + tmpquad[2]
        
        t1 = self.astStack.pop()
        t2 = self.astStack.pop()

        if isinstance(t1, tuple):
            if t1[1][2] == 'function' or t1[1][2] == 'function_undeclared':
                t1o = t1[1][0]
            else:
                t1o = t1[1]
        else:
            t1o = t1
        if isinstance(t2, tuple):
            t2o = t2[1]
        else:
            t2o = t2
        tmpt = (t1o, t2o)

        # Figuring out what operator (overloaded) to use
        if p[2] in [ '>>', '<<', '%', '&', '|', '^', '&&', '||']:
            if tmpt == ("int", "int"):
                tmp = "int (normal)"
                self.astStack.push((None, "int", "identifier"))
                self.scopeStack.addtoTop([tmpquad_expr, 'int'], tmpquad[0] )
                p[2] == 'i'
            else:
                self._raise_error(self.clex.lexer.lineno, "Invalid operands " + str(t1o) + " and " + str(t2o)  + " for operator " + p[2] )
        elif tmpt == ("float", "float"):
            tmp  = "float"
            self.astStack.push((None, "float", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'float'], tmpquad[0] )
            p[2] += 'f'
            tmpquad[3] = p[2]
        elif tmpt == ("int", "int"):
            tmp = "int"
            self.astStack.push((None, "int", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'int'], tmpquad[0] )
            p[2] += 'i'
            tmpquad[3] = p[2]
        elif tmpt == ("float" , "int"):
            tmp = "float"
            self.astStack.push((None, "float", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'float'], tmpquad[0] )
            p[2] += 'f'
            tmpquad[3] = p[2]
        elif tmpt == ("int" , "float"):
            tmp = "float"
            self.astStack.push((None, "float", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'float'], tmpquad[0] )
            p[2] += 'f'
            tmpquad[3] = p[2]
        elif tmpt in [("int", "char"), ("char", "int")]:
            tmp = "int"
            self.astStack.push((None, "int", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'int'], tmpquad[0] )
            p[2] += 'i'
            tmpquad[3] = p[2]
        elif tmpt == ("char", "char"):
            tmp = "int"
            self.astStack.push((None, "int", "identifier"))
            self.scopeStack.addtoTop([tmpquad_expr, 'int'], tmpquad[0] )
            p[2] += 'i'
            tmpquad[3] = p[2]
        else:
            self._raise_error(self.clex.lexer.lineno, "Unmatchable types " + str(t1o) + " and " + str(t2o) + " with operator " + p[2])
            tmp = "ERROR: Unmatchable"
        tmp = "(" + tmp + ")"
        p[0].node = genNodes([ p[2] + tmp, p[1].node, p[3].node], self.dotFile)
        
        return tmpquad[0]

    def parse(self, text, filename='', debuglevel=0 ):
        """ Parses C code and returns an AST.

            text:
                A string containing the C source code

            filename:
                Name of the file being parsed (for meaningful
                error messages)

            debuglevel:
                Debug level to yacc
        """
        self.clex.filename = filename
        self.clex.reset_lineno()
        self._scope_stack = [dict()]
        self._last_yielded_token = None
        tmp = self.cparser.parse(
                input=text,
                lexer=self.clex,
                debug=debuglevel)

        self.normalstf.write("\nGLOBAL\n")
        globalScope = self.scopeStack.pop()
        self.scopeTree.addData(globalScope)

        for key, value in globalScope.items():
            self.stf.writerow([key, value])

        TACFile = open('../bin/TACFile.csv', 'w')
        for i in range(0, len(self.quadList)):
            TACFile.write(str(i + 1))
            for j in range(0, 4):
                TACFile.write("\t" + str(self.quadList[i][j]))
            TACFile.write("\n")
        
        # print("________________________________")
        # self.scopeTree.printTree(self.scopeTree.root)
        # print("________________________________")
        genAsm(self.scopeTree, self.quadList, self.funcParamList)

        return tmp;

    def _lex_error_func(self, msg, line, column):
        self._parse_error(msg, self._coord(line, column))

    def _lex_on_lbrace_func(self):
        if self.function_definition_flag != True:
            self.scopeStack.push(dict())
            self.numScopes += 1
            scopeName = "UNSC" + str(self.numScopes) #UNSC = UNnamed SCope
            self.quadList.append(genQuad(scopeName, '', 'scope_begin'))
            self.scopeNames.push(scopeName)
            self.scopeTree.createEmptyNode(scopeName)
            # print("SCOPE_BEGIN")
            # print("{")
        else:
            self.function_definition_flag = False

    def _lex_on_rbrace_func(self):
        scope_dict = self.scopeStack.top()
        scopeName = self.scopeNames.top()
        self.normalstf.write("\n" + scopeName +  "\n")
        for key, value in scope_dict.items():
            self.stf.writerow([key, value])

        # self.scopeTree.addData(scope_dict)
        # self.scopeTree.changeCurrent(self.scopeTree.parent())
        # self.quadList.append(genQuad(scopeName, '', 'scope_exit'))

    def _lex_type_lookup_func(self, name):
        pass
        # """ Looks up types that were previously defined with
            # typedef.
            # Passed to the lexer for recognizing identifiers that
            # are types.
        # """
        # is_type = self._is_type_in_scope(name)
        # return is_type

    def _get_yacc_lookahead_token(self):
        # """ We need access to yacc's lookahead token in certain cases.
            # This is the last token yacc requested from the lexer, so we
            # ask the lexer.
        # """
        return self.clex.last_token

    ## Precedence and associativity of operators
    precedence = (
        ('left', 'LOR'),
        ('left', 'LAND'),
        ('left', 'OR'),
        ('left', 'XOR'),
        ('left', 'AND'),
        ('left', 'EQ', 'NE'),
        ('left', 'GT', 'GE', 'LT', 'LE'),
        ('left', 'RSHIFT', 'LSHIFT'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE', 'MOD')
    )

    ## Grammar productions
    ## Implementation of the BNF defined in K&R2 A.13

    # Wrapper around a translation unit, to allow for empty input.
    # Not strictly part of the C99 Grammar, but useful in practice.
    #
    def p_translation_unit_or_empty(self, p):
        """ translation_unit_or_empty   : translation_unit
                                        | empty
        """
        if p[1] != None:
            p[0] = Attributes();
            p[0].node = genNodes(["translation_unit_or_empty", p[1].node], self.dotFile)
        else:
            p[0] = Attributes(); 
            p[0].node = genNodes(["translation_unit_or_empty", 'empty'], self.dotFile)
        

    def p_translation_unit_1(self, p):
        """ translation_unit    : external_declaration
        """
        p[0] = Attributes();
        p[0].node = genNodes(["translation_unit", p[1].node], self.dotFile);

    def p_translation_unit_2(self, p):
        """ translation_unit    : translation_unit external_declaration
        """
        p[0] = Attributes();
        p[0].node = genNodes(["translation_unit", p[1].node, p[2].node], self.dotFile)

    # Declarations always come as lists (because they can be
    # several in one line), so we wrap the function definition
    # into a list as well, to make the return value of
    # external_declaration homogenous.
    #
    def p_external_declaration_1(self, p):
        """ external_declaration    : function_definition
        """
        p[0] = Attributes(); p[0].node = genNodes(["external_declaration", p[1].node], self.dotFile)

    def p_external_declaration_2(self, p):
        """ external_declaration    : declaration
        """
        p[0] = Attributes(); p[0].node = genNodes(["external_declaration", p[1].node], self.dotFile)

    def p_external_declaration_3(self, p):
        """ external_declaration    : pp_directive
                                    | pppragma_directive
        """
        p[0] = Attributes(); p[0].node = genNodes(["external_declaration",p[1].node], self.dotFile)

    def p_external_declaration_4(self, p):
        """ external_declaration    : SEMI
        """
        p[0] = Attributes(); p[0].node = genNodes(["external_declaration", [p[1]]], self.dotFile)

    def p_pp_directive(self, p):
        """ pp_directive  : PPHASH
        """
        p[0] = Attributes(); p[0].node = genNodes(["pp_directive", [p[1].node]], self.dotFile)

    def p_pppragma_directive(self, p):
        """ pppragma_directive      : PPPRAGMA
                                    | PPPRAGMA PPPRAGMASTR
        """
        p[0] = Attributes(); p[0].node = genNodes(["pppragma_directive", [p[1].node]], self.dotFile)

    def p_function_definition_1(self, p):
        """ function_definition : id_declarator declaration_list_opt compound_statement
        """
        p[0] = Attributes();
        if p[2] != None:
            p[0].node = genNodes(["function_definition", p[1].node, p[2].node,p[3].node], self.dotFile)
        else:
            p[0].node = genNodes(["function_definition", p[1].node, p[3].node], self.dotFile)

    def p_function_definition_2(self, p):
        """ function_definition : function_declarator compound_statement
        """
        if p[2] == None or p[2].node == None:
            p[0] = Attributes(); p[0].node = p[1].node
        else:
            p[0] = Attributes(); p[0].node = genNodes(["function_definition", p[1].node, p[2].node],self.dotFile)
        self.quadList.append(genQuad('','','retq_func_exit'))

    def p_function_declarator(self, p):
        """function_declarator : declaration_specifiers id_declarator declaration_list_opt
        """
        tmp = []
        if self.evalStack.top() == '$': #function with no parameters _RUSHAB CHANGE
            # FUNCname = self.evalStack.stack[self.evalStack.size() - 2]
            del self.quadList[len(self.quadList) -1]
            # self.quadList.append(genQuad(FUNCname, '', 'func_begin_1'))
            self.evalStack.pop()
        
        paramNameList = []
        if self.evalStack.top() == '$func_has_parameters':
            self.evalStack.pop()
             
            i = self.evalStack.pop()
            if i != '$':
                paramNameList.append(i)
            while(i != '$'):
                tmppop = self.evalStack.pop()
                arraycheck = 0
                if isinstance(tmppop, tuple) and tmppop[0] =="array":
                    arraycheck = 1
                    tmp_array = tmppop
                    tmppop = self.evalStack.pop()

                tmp_pointer_count = 0
                while tmppop == ("*", "pointer"):
                    tmp_pointer_count += 1
                    tmppop = self.evalStack.pop()

                if arraycheck == 1:
                    tmppop = (tmppop, "array", tmp_array[1])
                if tmp_pointer_count >= 1:
                    tmppop = ("pointer", tmppop)

                for i in range(0, tmp_pointer_count - 1):
                    tmppop = ("pointer", tmppop)

                tmp.append(tmppop)
                i = self.evalStack.pop()
    
                if i == ',':
                    i = self.evalStack.pop()
                if i not in ['$', ',']:
                    paramNameList.append(i)

        t1 = self.evalStack.pop()
        t2 = self.evalStack.pop()

        #ADD func_begin to quadruples
        FUNCname = t1
        self.quadList.append(genQuad(FUNCname, '', 'func_begin')) # generate quadruple to push parameter on stack
        self.funcParamList[t1] = paramNameList

        if t2 == ("*", "pointer"):
            t2 = ("pointer", self.evalStack.pop())
        tmp.reverse()

        #Ingnoring function overloading
        tmpSearch = self.scopeStack.searchBelowTop(t1)
        if tmpSearch:
            if tmpSearch[0] == None and isinstance(tmpSearch[1], tuple) and isinstance(tmpSearch[1][1], list) and tmpSearch[1][2] == 'function_undeclared': #Forward declaration
                checkArgList = 0
                if len(tmp) != len(tmpSearch[1][1]):
                    self._raise_error(p.lineno(1), "Definition incompatible with its forward declaration: function " + t1 + ": number of arguments must be the same" )

                for i in range(0, len(tmp)):
                    if tmpSearch[1][1][i] != tmp[i]:
                        checkArgList = 1
                        break

                if checkArgList == 1:
                    self._raise_error(p.lineno(1), "Definition incompatible with its forward declaration: function " + t1 + ": argument types must be the same")

                if t2 != tmpSearch[1][0]:
                    self._raise_error(p.lineno(1), "Definition incompatible with its forward declaration: function " + t1 + ": return type must be the same")
                self.scopeStack.addBelowTop([None, (t2,tmp), "function"], t1)
                self.scopeTree.changeNodeName(self.scopeTree.current, t1)
            else:
                self._raise_error(self.clex.lexer.lineno, "Redefinition_4 of variable " + t1)
        else:
            self.scopeStack.addBelowTop([None, (t2, tmp, "function")], t1)
            self.scopeTree.changeNodeName(self.scopeTree.current, t1)

        self.scopeNames.push(t1)

        parameter_node_list = ["function_declarator", [t1], [str(t2) + "\n(return type)"]]
        for i in tmp:
            parameter_node_list.append([str(i)])
        p[0] = Attributes();
        p[0].node = genNodes(parameter_node_list, self.dotFile)

    def p_statement(self, p):
        """ statement   : labeled_statement
                        | expression_statement
                        | compound_statement
                        | selection_statement
                        | iteration_statement
                        | jump_statement
                        | pppragma_directive
                        | PRINTF LPAREN identifier RPAREN SEMI
                        | PRINTF LPAREN constant RPAREN SEMI
                        | SCANF LPAREN identifier RPAREN SEMI
        """
        # print("statement")
        if len(p) == 6:
            # print(self.scopeNames.stack)
            idname = self.evalStack.top()
            # print(idname, p[3].value)
            # print(self.scopeStack.stack)
            idtype = self.scopeStack.search(idname)[1]

            # if p[3].value != None and str(p[1]) == 'printf':
                    # self.quadList.append(genQuad(p[3].value, idtype, 'printf'))

            # elif p[3].value == None and str(p[1]) == 'printf':
                    # self.quadList.append(genQuad(idname, idtype, 'printf'))

            if str(p[1]) == 'printf':
                    self.quadList.append(genQuad(idname, idtype, 'printf'))

            elif str(p[1]) == 'scanf':
                self.quadList.append(genQuad(idname, idtype, 'scanf'))

            p[0] = Attributes()
            p[0].node = p[3].node
        else:
            p[0] = Attributes();
            p[0].node = p[1].node
            p[0].breakList = p[1].breakList
            concat(p[0].continueList, p[1].continueList)
            concat(p[0].breakList, p[1].breakList)


    def p_decl_body(self, p):
        """ decl_body : declaration_specifiers init_declarator_list_opt
                      | declaration_specifiers_no_type id_init_declarator_list_opt
        """
        if not self.evalStack.empty() and self.evalStack.top() != '$':
            self._extract_id_list_to_scope(self.clex.lexer.lineno)
            if p[1].node == None:
                p[0] = Attributes();
                p[0].node = p[2].node
            else:
                if p[2] == None or p[2].node == None:
                    p[0] = Attributes();
                    p[0].node = genNodes(["decl_body_1", p[1].node], self.dotFile)
                else:
                    p[0] = Attributes();
                    p[0].node = genNodes(["decl_body_2", p[1].node, p[2].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = p[1].node


    def p_declaration(self, p):
        """ declaration : decl_body SEMI
        """
        if p[1].node != None:
            p[0] = Attributes();
            p[0].node = p[1].node
        else:
            p[0] = Attributes();
            p[0].node = genNodes(["DECLARATION"], self.dotFile)

    def p_declaration_list(self, p):
        """ declaration_list    : declaration
                                | declaration_list declaration
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = genNodes(["declaration_list", p[1].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["declaration_list",p[1].node,p[2].node], self.dotFile)

    def p_declaration_specifiers_no_type_1(self, p):
        """ declaration_specifiers_no_type  : type_qualifier declaration_specifiers_no_type_opt
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers_no_type", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_no_type_2(self, p):
        """ declaration_specifiers_no_type  : storage_class_specifier declaration_specifiers_no_type_opt
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers_no_type", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_no_type_3(self, p):
        """ declaration_specifiers_no_type  : function_specifier declaration_specifiers_no_type_opt
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers_no_type", p[1].node, p[2].node], self.dotFile)


    def p_declaration_specifiers_1(self, p):
        """ declaration_specifiers  : declaration_specifiers type_qualifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_2(self, p):
        """ declaration_specifiers  : declaration_specifiers storage_class_specifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_3(self, p):
        """ declaration_specifiers  : declaration_specifiers function_specifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_4(self, p):
        """ declaration_specifiers  : declaration_specifiers type_specifier_no_typeid
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers", p[1].node, p[2].node], self.dotFile)

    def p_declaration_specifiers_5(self, p):
        """ declaration_specifiers  : type_specifier
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_declaration_specifiers_6(self, p):
        """ declaration_specifiers  : declaration_specifiers_no_type type_specifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["declaration_specifiers", p[1].node, p[2].node], self.dotFile)

    def p_storage_class_specifier(self, p):
        """ storage_class_specifier : AUTO
                                    | REGISTER
                                    | STATIC
                                    | EXTERN
                                    | TYPEDEF
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_function_specifier(self, p):
        """ function_specifier  : INLINE
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_type_specifier_no_typeid(self, p):
        """ type_specifier_no_typeid  : VOID
                                      | _BOOL
                                      | CHAR
                                      | SHORT
                                      | INT
                                      | LONG
                                      | FLOAT
                                      | DOUBLE
                                      | _COMPLEX
                                      | SIGNED
                                      | UNSIGNED
                                      | __INT128
        """
        self.evalStack.push("$")
        self.evalStack.push(p[1])
        p[0] = Attributes(p[1])

    def p_type_specifier(self, p):
        """ type_specifier  : typedef_name
                            | enum_specifier
                            | struct_or_union_specifier
                            | type_specifier_no_typeid
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_type_qualifier(self, p):
        """ type_qualifier  : CONST
                            | RESTRICT
                            | VOLATILE
        """
        p[0] = Attributes(); p[0].node = genNodes(["type_qualifier", [p[1]]], self.dotFile)

    def p_init_declarator_list(self, p):
        """ init_declarator_list    : init_declarator
                                    | init_declarator_list COMMA init_declarator
        """
        if len(p) == 4:
            if p[1] != None and p[3] != None:
                p[0] = Attributes(); p[0].node = genNodes(["init_declarator_list", p[1].node, [p[2]], p[3].node], self.dotFile)
            elif p[1] != None:
                p[0] = Attributes(); p[0].node = p[1].node
            elif p[3] != None:
                p[0] = Attributes(); p[0].node = p[3].node
            else:
                p[0] = Attributes(); p[0].node = p[1]
        else:
            if p[1] != None:
                p[0] = Attributes(); p[0].node = genNodes(["init_declarator_list", p[1].node], self.dotFile)
            else:
                p[0] = None

    ### Returns a {decl=<declarator> : init=<initializer>} dictionary
    ### If there's no initializer, uses None
    ###
    def p_init_declarator(self, p):
        """ init_declarator : declarator
                            | declarator EQUALS initializer
        """
        if len(p) == 4:
            t1 = self.evalStack.pop()
            t2 = self.evalStack.top()
            tmp = self.scopeStack.searchTop(t2)

            # self.quadList.append(genQuad(t1, "", p[2], t2))

            if tmp != False and tmp[1] != None:
                self._raise_error(self.clex.lexer.lineno, "Redefinition_5 of " + str(t2))
            else:
                self.scopeStack.addtoTop([t1, None], t2)

            # if type(t1) not in ['int', 'float', 'char']:
                # tmp = self.scopeStack.search(t1)
                # if type(tmp[0]) in ['int' , 'float', 'char']:
                    # self.scopeStack.changeValue(tmp[0], t2)
                    # self.quadList.append(genQuad(tmp[0], "", p[2], t2)) #changeU
                # else:
                    # self.quadList.append(genQuad(t1, "", p[2], t2))

            #############################
            self.quadList.append(genQuad(t1, "", p[2], t2))
            ##############################
            p[0] = Attributes(); 
            p[0].node = genNodes([p[2], p[1].node, p[3].node], self.dotFile)
        else:
            t2 = self.evalStack.top()
            if t2 == 'FORWARD_DECLARATION':
                return
            tmp =self.scopeStack.searchTop(t2)

            if tmp !=False and tmp[1].node != None:
                self._raise_error(self.clex.lexer.lineno, "Redefinition_6 of " + str(t2))
            else:
                self.scopeStack.addtoTop([ None, None], t2)

    def p_id_init_declarator_list(self, p):
        """ id_init_declarator_list    : id_init_declarator
                                       | id_init_declarator_list COMMA init_declarator
        """
        if len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["id_init_declarator_list",p[1].node, [p[2].node], p[3].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["id_init_declarator_list", p[1].node], self.dotFile)

    def p_id_init_declarator(self, p):
        """ id_init_declarator : id_declarator
                               | id_declarator EQUALS initializer
        """
        if len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["id_init_declarator", p[1].node, [p[2].node], p[3].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["id_init_declarator", p[1].node], self.dotFile)

    def p_specifier_qualifier_list_1(self, p):
        """ specifier_qualifier_list    : specifier_qualifier_list type_specifier_no_typeid
        """
        p[0] = Attributes(); p[0].node = genNodes(["specifier_qualifier_list", p[1].node, p[2].node], self.dotFile)

    def p_specifier_qualifier_list_2(self, p):
        """ specifier_qualifier_list    : specifier_qualifier_list type_qualifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["specifier_qualifier_list", p[1].node,p[2].node], self.dotFile)

    def p_specifier_qualifier_list_3(self, p):
        """ specifier_qualifier_list  : type_specifier
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_specifier_qualifier_list_4(self, p):
        """ specifier_qualifier_list  : type_qualifier_list type_specifier
        """
        p[0] = Attributes(); p[0].node = genNodes(["specifier_qualifier_list", p[1].node, p[2].node], self.dotFile)

    ### TYPEID is allowed here (and in other struct/enum related tag names), because
    ### struct/enum tags reside in their own namespace and can be named the same as types
    ###
    def p_struct_or_union_specifier_1(self, p):
        """ struct_or_union_specifier   : struct_or_union TYPEID
        """
        if self.evalStack.top() == "$Struct_declarations":
            self.evalStack.pop()
        self.evalStack.push(("struct", p[2]))
        p[0] = Attributes(); p[0].node = genNodes(["struct_or_union_specifier", p[1].node, [p[2]]], self.dotFile)

    def p_struct_or_union_specifier_1_b(self, p):
        """ struct_or_union_specifier   : struct_or_union ID
        """
        self.evalStack.push(p[2])
        p[0] = Attributes(); p[0].node = genNodes(["struct_or_union_specifier", p[1].node, [p[2]]], self.dotFile)

    def p_struct_or_union_specifier_2(self, p):
        """ struct_or_union_specifier : struct_or_union brace_open struct_declaration_list brace_close
        """
        p[0] = Attributes(); p[0].node = genNodes(["struct_or_union_specifier", p[1].node, p[2].node, p[3].node, p[4].node], self.dotFile)

    def p_struct_or_union_specifier_3(self, p):
        """ struct_or_union_specifier   : struct_or_union ID new_struct_rule_3
                                        | struct_or_union TYPEID new_struct_rule_3
        """
        self.scopeNames.push("STRUCT " + p[1].node)
        structVars = self.scopeStack.pop()
        # self.scopeTree.addData(structVars)
        
        offset = 0
        for i in structVars:
            structVars[i].append(offset)
            size = returnWidth(structVars[i][1])
            offset += size
        
        self.scopeStack.addtoTop([structVars, "struct"], p[2])

        p[0] = Attributes(); 
        p[0].node = genNodes(["struct_or_union_specifier", p[1].node, [p[2]], p[3].node], self.dotFile)

    def p_new_struct_rule_3(self, p):
        """ new_struct_rule_3 : brace_open struct_declaration_list brace_close
        """
        self.scopeStack.push(dict())
        # self.scopeTree.createEmptyNode()
        
        while self.evalStack.top() != '$Struct_declarations':
            self._extract_id_list_to_scope(self.clex.lexer.lineno)
        # self.scopeTree.changeCurrent(self.scopeTree.parent())
        self.evalStack.pop()
        # try:
            # if self.evalStack.top() == '$':
                # self.evalStack.pop()
        # except:
            # time_waste =1
        p[0] = Attributes(); p[0].node = genNodes(["new_struct_rule_3",  p[2].node], self.dotFile)

    def p_struct_or_union(self, p):
        """ struct_or_union : STRUCT
                            | UNION
        """
        self.evalStack.push("$Struct_declarations")
        p[0] = Attributes(); p[0].node = genNodes(["struct_or_union",[p[1]]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[1].node

    # Combine all declarations into a single list
    #
    def p_struct_declaration_list(self, p):
        """ struct_declaration_list     : struct_declaration
                                        | struct_declaration_list struct_declaration
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = p[1].node or []
        else:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declaration_list", p[1].node, p[2].node], self.dotFile)

    def p_struct_declaration_1(self, p):
        """ struct_declaration : specifier_qualifier_list struct_declarator_list_opt SEMI
        """
        if p[2].node is not None:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declaration" , p[1].node, p[2].node, [p[3]]], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declaration" , p[1].node, [p[3]]], self.dotFile)

    def p_struct_declaration_2(self, p):
        """ struct_declaration : SEMI
        """
        p[0] = Attributes(); p[0].node = genNodes(["struct_declaration", [p[1]]], self.dotFile)

    def p_struct_declarator_list(self, p):
        """ struct_declarator_list  : struct_declarator
                                    | struct_declarator_list COMMA struct_declarator
        """
        if len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declarator_list", p[1].node, [p[2]], p[3].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = p[1].node

    # struct_declarator passes up a dict with the keys: decl (for
    # the underlying declarator) and bitsize (for the bitsize)
    #
    def p_struct_declarator_1(self, p):
        """ struct_declarator : declarator
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_struct_declarator_2(self, p):
        """ struct_declarator   : declarator COLON constant_expression
                                | COLON constant_expression
        """
        if len(p) > 3:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declarator", p[1].node, [p[2].node], p[3].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["struct_declarator", [p[1].node], p[2].node], self.dotFile)

    def p_enum_specifier_1(self, p):
        """ enum_specifier  : ENUM ID
                            | ENUM TYPEID
        """
        p[0] = Attributes(); p[0].node = genNodes(["enum_specifier", [p[1]], [p[2]]], self.dotFile)

    def p_enum_specifier_2(self, p):
        """ enum_specifier  : ENUM brace_open enumerator_list brace_close
        """
        p[0] = Attributes(); p[0].node = genNodes(["enum_specifier", [p[1].node], p[2].node, p[3].node, p[4].node], self.dotFile)

    def p_enum_specifier_3(self, p):
        """ enum_specifier  : ENUM ID brace_open enumerator_list brace_close
                            | ENUM TYPEID brace_open enumerator_list brace_close
        """
        p[0] = Attributes(); p[0].node = genNodes(["enum_specifier", [p[1].node], [p[2].node], p[3].node, p[4].node, p[5].node], self.dotFile)

    def p_enumerator_list(self, p):
        """ enumerator_list : enumerator
                            | enumerator_list COMMA
                            | enumerator_list COMMA enumerator
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = genNodes(["enumerator_list", p[1].node], self.dotFile)
        elif len(p) == 3:
            p[0] = Attributes(); p[0].node = genNodes(["enumerator_list", p[1].node, [p[2].node]], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["enumerator_list", p[1].node, [p[2].node], p[3].node], self.dotFile)

    def p_enumerator(self, p):
        """ enumerator  : ID
                        | ID EQUALS constant_expression
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = genNodes(["enumerator", [p[1].node]], self.dotFile)
            # enumerator = c_ast.Enumerator(
                        # p[1].node, None,
                        # self._coord(p.lineno(1)))
        else:
            p[0] = Attributes(); p[0].node = genNodes(["enumerator", [p[1].node], [p[2].node], p[3].node], self.dotFile)
            # enumerator = c_ast.Enumerator(
                        # p[1].node, p[3].node,
                        # self._coord(p.lineno(1)))
        # self._add_identifier(enumerator.name, enumerator.coord)

        # p[0] = Attributes(); p[0].node = enumerator

    def p_declarator(self, p):
        """ declarator  : id_declarator
                        | typeid_declarator
        """
        # p[0] = Attributes(); p[0].node = genNodes(["declarator", p[1].node], self.dotFile)
        if p[1] != None:
            p[0] = Attributes(); p[0].node = p[1].node

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_xxx_declarator_1(self, p):
        """ xxx_declarator  : direct_xxx_declarator
        """
        if p[1] != None:
            p[0] = Attributes(); p[0].node = p[1].node

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_xxx_declarator_2(self, p):
        """ xxx_declarator  : pointer direct_xxx_declarator
        """
        p[0] = Attributes(); p[0].node = p[1].node

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_1(self, p):
        """ direct_xxx_declarator   : yyy
        """
        self.evalStack.push(p[1])
        # p[0] = Attributes(); p[0].node = genNodes(["direct_xxx_declarator", [p[1].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = genNodes([p[1].node], self.dotFile)
        p[0] = Attributes(); p[0].node = p[1]

        # p[0] = Attributes(); p[0].node = c_ast.TypeDecl(
            # declname=p[1].node,
            # type=None,
            # quals=None,
            # coord=self._coord(p.lineno(1)))

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'))
    def p_direct_xxx_declarator_2(self, p):
        """ direct_xxx_declarator   : LPAREN xxx_declarator RPAREN
        """
        # p[0] = Attributes(); p[0].node = genNodes(["direct_xxx_declarator_2", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[1].node
        p[0] = Attributes(); p[0].node = p[2].node

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_3(self, p): # grammar changed to avoid dangling nodes for arrays
        """ direct_xxx_declarator   : direct_xxx_declarator lbr type_qualifier_list_opt assignment_expression_opt RBRACKET
        """
        t1 = self.evalStack.pop()
        t2 = self.evalStack.pop()
        t3 = self.evalStack.pop()
        if isinstance(t3, tuple) and t3[0] == "array":
            t3[1].append(t1)
            self.evalStack.push(t3)
        else:
            self.evalStack.push(t3)
            self.evalStack.push(("array", [t1]))
        self.evalStack.push(t2)

        p[0] = Attributes(); p[0].node = p[1].node

    def p_lbr(self, p):
        """ lbr : LBRACKET
        """
        self.evalStack.push("ISARRAY")


    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_4(self, p):
        """ direct_xxx_declarator   : direct_xxx_declarator LBRACKET STATIC type_qualifier_list_opt assignment_expression RBRACKET
        """
                                    # | direct_xxx_declarator LBRACKET type_qualifier_list STATIC assignment_expression RBRACKET
        # p[0] = Attributes(); p[0].node = genNodes(["direct_xxx_declarator", p[1].node, [p[2].node], [p[3].node], p[4].node, p[5].node, [p[6].node.node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[1].node
        ### Using slice notation for PLY objects doesn't work in Python 3 for the
        ### version of PLY embedded with pycparser; see PLY Google Code issue 30.
        ### Work around that here by listing the two elements separately.
        # listed_quals = [item if isinstance(item, list) else [item]
            # for item in [p[3].node,p[4].node]]
        # dim_quals = [qual for sublist in listed_quals for qual in sublist
            # if qual is not None]
        # arr = c_ast.ArrayDecl(
            # type=None,
            # dim=p[5].node,
            # dim_quals=dim_quals,
            # coord=p[1].node.coord)

        # p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=arr)

    ### Special for VLAs
    ###

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_5(self, p):
        """ direct_xxx_declarator   : direct_xxx_declarator     LBRACKET    type_qualifier_list_opt    TIMES   RBRACKET
        """
        # p[0] = Attributes(); p[0].node = genNodes(["direct_xxx_declarator", p[1].node, [p[2].node], p[3].node,  [p[4].node], [p[5].node]], self.dotFile)
        p[0] = Attributes(); p[0].node = p[1].node

        # arr = c_ast.ArrayDecl(
            # type=None,
            # dim=c_ast.ID(p[4].node, self._coord(p.lineno(4))),
            # dim_quals=p[3].node if p[3].node != None else [],
            # coord=p[1].node.coord)

        # p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=arr)

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_6(self, p):
        """ direct_xxx_declarator   : direct_xxx_declarator LPAREN parameter_type_list RPAREN
        """
        if self.evalStack.top() == '$func_has_parameters' and self.evalStack.stack[len(self.evalStack.stack) - 3] in ['$', ',']:
            tmp = []
            i = self.evalStack.pop()
            while(i != '$'):
                tmp.append(self.evalStack.pop())
                i = self.evalStack.pop()
            tmp.reverse()
            t1 = self.evalStack.pop()
            t2 = self.evalStack.pop()
            tmpid = (None,(t2, tmp))
            if self.scopeStack.searchTop(t1):
                self._raise_error(self.clex.lexer.lineno, "Redfinition of variable " + t1)
            else:
                self.scopeStack.addtoTop(tmpid, t1)
        p[0] = Attributes(); p[0].node = p[1].node


        #func = c_ast.FuncDecl(
         #    args=p[3].node,
          #   type=None,
           #  coord=p[1].node.coord)

        ### To see why _get_yacc_lookahead_token is needed, consider:
        ###   typedef char TT;
        ###   void foo(int TT) { TT = 10; }
        ### Outside the function, TT is a typedef, but inside (starting and
        ### ending with the braces) it's a parameter.  The trouble begins with
        ### yacc's lookahead token.  We don't know if we're declaring or
        ### defining a function until we see LBRACE, but if we wait for yacc to
        ### trigger a rule on that token, then TT will have already been read
        ### and incorrectly interpreted as TYPEID.  We need to add the
        ### parameters to the scope the moment the lexer sees LBRACE.
        ###
         #if self._get_yacc_lookahead_token().type == "LBRACE":
          #   if func.args is not None:
           #      for param in func.args.params:
            #         if isinstance(param, c_ast.EllipsisParam): break
             #        self._add_identifier(param.name, param.coord)

         #p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=func)

    ##############ADDED BY RUSHAB FROM p_direct_xxx_declarator_4##########################
    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_7(self, p):
        """ direct_xxx_declarator   : direct_xxx_declarator LBRACKET type_qualifier_list STATIC assignment_expression RBRACKET
        """
        p[0] = Attributes(); p[0].node = p[1].node

    @parameterized(('id', 'ID'), ('typeid', 'TYPEID'), ('typeid_noparen', 'TYPEID'))
    def p_direct_xxx_declarator_8(self, p):
        """ direct_xxx_declarator   : direct_xxx_declarator LPAREN identifier_list_opt RPAREN
        """
        self.evalStack.push('$')

        if self.clex.last_token.value == ';':
            self.evalStack.push("FORWARD_DECLARATION")
        p[0] = None
    ###############ADDED TILL HERE###########################################################

    def p_pointer(self, p):
        """ pointer : TIMES type_qualifier_list_opt
                    | TIMES type_qualifier_list_opt pointer
        """
        self.evalStack.push(('*', "pointer"))
        if len(p) == 3:
            p[0] = Attributes(); p[0].node = p[1]
        else:
            p[0] = Attributes(); p[0].node = p[1]

    def p_type_qualifier_list(self, p):
        """ type_qualifier_list : type_qualifier
                                | type_qualifier_list type_qualifier
        """
        if len(p) == 3:
            p[0] = Attributes(); p[0].node = genNodes(["type_qualifier_list", p[1].node, p[2].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["type_qualifier_list", p[1].node], self.dotFile)

    def p_parameter_type_list(self, p):
        """ parameter_type_list : parameter_list
                                | parameter_list COMMA ELLIPSIS
        """

        if self.evalStack.top() == 'FORWARD_DECLARATION':
            return

        #multiple parameters
        i = len(self.evalStack.stack) - 2

        while self.evalStack.stack[i] == ("*", "pointer"):
            i -= 1
        i -= 1

        if self.evalStack.stack[i] == ',':
            self.scopeStack.push(dict())
            self.scopeTree.createEmptyNode()

            self.function_definition_flag = True
            #create duplicates of parameters and push them to include them again in function definition
            i = len(self.evalStack.stack) - 1
            tmp = []
            while(self.evalStack.stack[i] != '$'):
                tmp.append(self.evalStack.stack[i])
                i = i - 1
            tmp.append('$')

            tmp.append('$func_has_parameters')
            tmp.reverse()
            self.evalStack.stack.extend(tmp)

            #add parameters to scope
            self._extract_id_list_to_scope( self.clex.lexer.lineno )

        else: #single parameter
            self.function_definition_flag = True
            self.scopeStack.push(dict())
            self.scopeTree.createEmptyNode()

            t1 = self.evalStack.pop()
            t2 = self.evalStack.pop()

            arraycheck = 0

            if isinstance(t2, tuple) and t2[0] == "array":
                arraycheck = 1
                tmp_array = t2
                t2 = self.evalStack.pop()

            tmp_pointer_count = 0
            while t2 == ("*", "pointer"):
                tmp_pointer_count += 1
                t2 = self.evalStack.pop()

            tmp_idtype = t2
            if arraycheck == 1:
                t2 = (tmp_idtype, "array", tmp_array[1] )

            if tmp_pointer_count >= 1:
                t2 = ("pointer", t2)

            for i in range(0,tmp_pointer_count - 1):
                t2 = ("pointer", t2)

            self.scopeStack.addtoTop( [None, t2], t1)

            #Add Function Start to TAC
            FUNCname = self.evalStack.stack[self.evalStack.size() - 2]
            # self.quadList.append(genQuad(FUNCname, '', 'func_begin'))

            #push things back on evalStack
            self.evalStack.push(tmp_idtype)
            for i in range(0, tmp_pointer_count):
                self.evalStack.push(("*", "pointer"))
            if arraycheck == 1:
                self.evalStack.push(tmp_array)
            self.evalStack.push(t1)

            self.evalStack.push('$func_has_parameters')

        if len(p) > 2:
            p[0] = Attributes(); p[0].node = p[1].node
        else:
            p[0] = Attributes(); p[0].node = p[1].node

    def p_parameter_list(self, p):
        """ parameter_list  : parameter_declaration
                            | parameter_list COMMA parameter_declaration
        """
        if len(p) == 2: # single parameter
            p[0] = Attributes(); p[0].node = p[1].node
        else:
            p[0] = Attributes(); p[0].node = p[1].node
            t1 = self.evalStack.pop()
            t2 = self.evalStack.pop()
            tmp = []
            tmpcheck = 0

            arraycheck = 0
            if isinstance(t2, tuple) and t2[0] == "array":
                arraycheck = 1
                tmp_array = t2
                t2 = self.evalStack.pop()

            while t2 == ("*", "pointer"):
                tmp.append(t2)
                t2 = self.evalStack.pop()
                tmpcheck = 1

            if tmpcheck == 0:
                t3 = self.evalStack.pop()
            else:
                t3 = t2
                t2 = tmp
            t4 = self.evalStack.pop()

            if arraycheck == 1:
                tmp.append(tmp_array)
            if t4 == '$':
                self.evalStack.push(p[2])
                self.evalStack.push(t3)
                for i in range(0, len(tmp)):
                    self.evalStack.push(tmp[i])
                self.evalStack.push(t1)
            elif t3 == "$":
                self.evalStack.push(t4)
                self.evalStack.push(p[2])
                self.evalStack.push(t2)
                if arraycheck == 1:
                    self.evalStack.push(tmp_array)
                self.evalStack.push(t1)
            elif t2 == "$":
                self.evalStack.push(t4)
                self.evalStack.push(t3)
                self.evalStack.push(p[2])
                self.evalStack.push(t1)
            else:
                self.evalStack.push(t3)
                self.evalStack.push(t2)
                self.evalStack.push(t1)


    ### From ISO/IEC 9899:TC2, 6.7.5.3.11:
    ### "If, in a parameter declaration, an identifier can be treated either
    ###  as a typedef name or as a parameter name, it shall be taken as a
    ###  typedef name."
    ###
    ### Inside a parameter declaration, once we've reduced declaration specifiers,
    ### if we shift in an LPAREN and see a TYPEID, it could be either an abstract
    ### declarator or a declarator nested inside parens. This rule tells us to
    ### always treat it as an abstract declarator. Therefore, we only accept
    ### `id_declarator`s and `typeid_noparen_declarator`s.
    def p_parameter_declaration_1(self, p):
        """ parameter_declaration   : declaration_specifiers id_declarator
                                    | declaration_specifiers typeid_noparen_declarator
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_parameter_declaration_2(self, p):
        """ parameter_declaration   : declaration_specifiers abstract_declarator_opt
        """
        p[0] = Attributes(); p[0].node = p[1].node

        #NOT ALLOWING POINTERS AND ARRAYS FOR THE moment
        t1 = self.evalStack.pop()
        t2 = self.evalStack.pop()
        t3 = self.evalStack.pop()
        if t3 != 'FORWARD_DECLARATION':
            self.evalStack.push(t3)
        self.evalStack.push(t2)
        self.evalStack.push(t1)
        self.evalStack.push("FORWARD_DECLARATION")

    def p_identifier_list(self, p):
        """ identifier_list : identifier
                            | identifier_list COMMA identifier
        """
        if len(p) == 2: # single parameter
            p[0] = Attributes(); p[0].node = p[1].node
        else:
            p[0] = Attributes(); p[0].node = genNodes(["identifier_list", p[1].node, [p[2]], p[3].node], self.dotFile)
            p[0] = Attributes(); p[0].node = p[1].node

    def p_initializer_1(self, p):
        """ initializer : assignment_expression
        """
        # p[0] = Attributes(); p[0].node = genNodes(["initializer", p[1].node], self.dotFile)
        p[0] = Attributes(); p[0].node = p[1].node

    def p_initializer_2(self, p):
        """ initializer : brace_open initializer_list_opt brace_close
                        | brace_open initializer_list COMMA brace_close
        """
        if len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["initializer", p[1].node, p[2].node, p[3].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["initializer", p[1].node, p[2].node, [p[3].node], p[4].node], self.dotFile)
        # if p[2].node is None:
            # p[0] = Attributes(); p[0].node = c_ast.InitList([], self._coord(p.lineno(1)))
        # else:
            # p[0] = Attributes(); p[0].node = p[2].node

    def p_initializer_list(self, p):
        """ initializer_list    : designation_opt initializer
                                | initializer_list COMMA designation_opt initializer
        """
        if len(p) == 3: # single initializer
            p[0] = Attributes(); p[0].node = genNodes(["initializer_list", p[1].node, p[2].node], self.dotFile)
            # init = p[2].node if p[1].node is None else c_ast.NamedInitializer(p[1].node, p[2].node)
            # p[0] = Attributes(); p[0].node = c_ast.InitList([init], p[2].node.coord)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["initializer_list", p[1].node, [p[2].node],p[3].node, p[4].node], self.dotFile)
            # init = p[4].node if p[3].node is None else c_ast.NamedInitializer(p[3].node, p[4].node)
            # p[1].node.exprs.append(init)
            # p[0] = Attributes(); p[0].node = p[1].node

    def p_designation(self, p):
        """ designation : designator_list EQUALS
        """
        p[0] = Attributes(); p[0].node = genNodes(["designation",p[1].node, [p[2].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[1].node

    ### Designators are represented as a list of nodes, in the order in which
    ### they're written in the code.
    ###
    def p_designator_list(self, p):
        """ designator_list : designator
                            | designator_list designator
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = genNodes(["designator_list", p[1].node, p[2].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["designator_list", p[1].node, p[2].node], self.dotFile)
        # p[0] = Attributes(); p[0].node = [p[1].node] if len(p) == 2 else p[1].node + [p[2].node]

    def p_designator(self, p):
        """ designator  : LBRACKET constant_expression RBRACKET
                        | PERIOD identifier
        """
        if len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["designator", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["designator", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[2].node

    def p_type_name(self, p):
        """ type_name   : specifier_qualifier_list abstract_declarator_opt
        """
        p[0] = Attributes(); p[0].node = genNodes(["type_name", p[1].node, p[2].node], self.dotFile)
        # typename = c_ast.Typename(
            # name='',
            # quals=p[1].node['qual'],
            # type=p[2].node or c_ast.TypeDecl(None, None, None),
            # coord=self._coord(p.lineno(2)))

        # p[0] = Attributes(); p[0].node = self._fix_decl_name_type(typename, p[1].node['type'])

    def p_abstract_declarator_1(self, p):
        """ abstract_declarator     : pointer
        """
        p[0] = Attributes(); p[0].node = genNodes(["abstract_declarator", p[1].node], self.dotFile)
        # dummytype = c_ast.TypeDecl(None, None, None)
        # p[0] = Attributes(); p[0].node = self._type_modify_decl(
            # decl=dummytype,
            # modifier=p[1].node)

    def p_abstract_declarator_2(self, p):
        """ abstract_declarator     : pointer direct_abstract_declarator
        """
        p[0] = Attributes(); p[0].node = genNodes(["abstract_declarator", p[1].node, p[2].node], self.dotFile)
        # p[0] = Attributes(); p[0].node = self._type_modify_decl(p[2].node, p[1].node)

    def p_abstract_declarator_3(self, p):
        """ abstract_declarator     : direct_abstract_declarator
        """
        p[0] = Attributes(); p[0].node = genNodes(["abstract_declarator", p[1].node], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[1].node

    ### Creating and using direct_abstract_declarator_opt here
    ### instead of listing both direct_abstract_declarator and the
    ### lack of it in the beginning of _1 and _2 caused two
    ### shift/reduce errors.
    #
    def p_direct_abstract_declarator_1(self, p):
        """ direct_abstract_declarator  : LPAREN abstract_declarator RPAREN """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = p[2].node

    def p_direct_abstract_declarator_2(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET assignment_expression_opt RBRACKET
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", p[1].node, [p[2].node], p[3].node, [p[4].node]], self.dotFile)
        # arr = c_ast.ArrayDecl(
            # type=None,
            # dim=p[3].node,
            # dim_quals=[],
            # coord=p[1].node.coord)

        # p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=arr)

    def p_direct_abstract_declarator_3(self, p):
        """ direct_abstract_declarator  : LBRACKET assignment_expression_opt RBRACKET
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = c_ast.ArrayDecl(
            # type=c_ast.TypeDecl(None, None, None),
            # dim=p[2].node,
            # dim_quals=[],
            # coord=self._coord(p.lineno(1)))

    def p_direct_abstract_declarator_4(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET TIMES RBRACKET
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", p[1].node, [p[2].node], [p[3].node], [p[4].node]], self.dotFile)
        # arr = c_ast.ArrayDecl(
            # type=None,
            # dim=c_ast.ID(p[3].node, self._coord(p.lineno(3))),
            # dim_quals=[],
            # coord=p[1].node.coord)

        # p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=arr)

    def p_direct_abstract_declarator_5(self, p):
        """ direct_abstract_declarator  : LBRACKET TIMES RBRACKET
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", [p[1].node], [p[2].node], [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = c_ast.ArrayDecl(
            # type=c_ast.TypeDecl(None, None, None),
            # dim=c_ast.ID(p[3].node, self._coord(p.lineno(3))),
            # dim_quals=[],
            # coord=self._coord(p.lineno(1)))

    def p_direct_abstract_declarator_6(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LPAREN parameter_type_list_opt RPAREN
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", p[1].node, [p[2].node], p[3].node, [p[4].node]], self.dotFile)
        # func = c_ast.FuncDecl(
            # args=p[3].node,
            # type=None,
            # coord=p[1].node.coord)

        # p[0] = Attributes(); p[0].node = self._type_modify_decl(decl=p[1].node, modifier=func)

    def p_direct_abstract_declarator_7(self, p):
        """ direct_abstract_declarator  : LPAREN parameter_type_list_opt RPAREN
        """
        p[0] = Attributes(); p[0].node = genNodes(["direct_abstract_declarator", [p[1].node], p[2].node, [p[3].node]], self.dotFile)
        # p[0] = Attributes(); p[0].node = c_ast.FuncDecl(
            # args=p[2].node,
            # type=c_ast.TypeDecl(None, None, None),
            # coord=self._coord(p.lineno(1)))

    ### declaration is a list, statement isn't. To make it consistent, block_item
    ### will always be a list
    ###
    def p_block_item(self, p):
        """ block_item  : declaration
                        | statement
        """
        # print("block_item")
        p[0] = Attributes(); p[0].node = p[1].node
        concat(p[0].continueList, p[1].continueList)
        concat(p[0].breakList, p[1].breakList)

    ### Since we made block_item a list, this just combines lists
    ###
    def p_block_item_list(self, p):
        """ block_item_list : block_item
                            | block_item_list block_item
        """
        # print("block_item_list")
        if len(p) == 2:
            p[0] = Attributes();
            p[0].node = p[1].node
            concat(p[0].continueList, p[1].continueList)
            concat(p[0].breakList, p[1].breakList)
        else:
            p[0] = Attributes();
            p[0].node = genNodes(["block_item_list", p[1].node, p[2].node], self.dotFile)
            p[0].continueList = concat(p[1].continueList, p[2].continueList, 1)
            p[0].breakList = concat(p[1].breakList, p[2].breakList, 1)

    def p_compound_statement_1(self, p):
        """ compound_statement : brace_open block_item_list_opt brace_close """
        # print("compound_statement")
        if p[2] != None and p[2].node != None:
            p[0] = Attributes();
            p[0].node =  p[2].node
            concat(p[0].continueList, p[2].continueList)
            concat(p[0].breakList, p[2].breakList)
        else:
            p[0] = Attributes()
            p[0].node = genNodes(["EMPTY"], self.dotFile)

    def p_labeled_statement_1(self, p):
        """ labeled_statement : ID COLON statement """
        p[0] = Attributes(); p[0].node = genNodes(["labeled_statement", [p[1].node], [p[2].node], p[3].node], self.dotFile)

    def p_labeled_statement_2(self, p):
        """ labeled_statement : CASE constant_expression COLON statement """
        p[0] = Attributes(); p[0].node = genNodes(["labeled_statement", [p[1].node], p[2].node, [p[3].node], p[4].node], self.dotFile)

    def p_labeled_statement_3(self, p):
        """ labeled_statement : DEFAULT COLON statement """
        p[0] = Attributes(); p[0].node = genNodes(["labeled_statement", [p[1].node], [p[2].node], p[3].node], self.dotFile)

    def p_selection_statement_1(self, p):
        """ selection_statement : ifdef statement
        """
        # backpatch( self.quadList, p[1].falseList, len(self.quadList) + 1 )
        backpatch( self.quadList, p[1].falseList, len(self.quadList) )
        if p[2] == None:
            p[0] = Attributes(); p[0].node = genNodes(["IF", p[1].node], self.dotFile)
        else:
            p[0] = Attributes(); p[0].node = genNodes(["IF", p[1].node, p[2].node], self.dotFile)
            concat(p[0].continueList, p[2].continueList)
            concat(p[0].breakList, p[2].breakList)


    def p_ifdef(self, p):
        """ ifdef : IF LPAREN expression RPAREN
        """
        p[0] = Attributes()
        p[0].node = p[3].node
        if len(p[3].falseList) == 0:
            if p[3].value == None:
                prevInstrRes = self.quadList[len(self.quadList) - 1][0]
                self.quadList.append(genQuad(prevInstrRes, '_', 'jfalse' ))
                p[0].falseList.append(len(self.quadList) - 1 )
            else:
                try:
                    if float(p[3].value) == 0:
                        self.quadList.append(genQuad('', '', 'goto'))
                        p[0].falseList.append(len(self.quadList) - 1 )
                except:
                    pass
        concat(p[0].falseList, p[3].falseList)

    def p_selection_statement_2(self, p):
        """ selection_statement : ifdef statement ELSE elseMarker statement
        """
        p[0] = Attributes();
        p[0].node = genNodes(["IF-ELSE", p[1].node, p[2].node, p[5].node], self.dotFile)
        backpatch(self.quadList, p[1].falseList, p[4].next + 1)
        # backpatch(self.quadList, p[4].nextList, len(self.quadList) + 1)
        backpatch(self.quadList, p[4].nextList, len(self.quadList) )
        concat(p[0].continueList, p[2].continueList)
        concat(p[0].breakList, p[2].breakList)
        concat(p[0].continueList, p[5].continueList)
        concat(p[0].breakList, p[5].breakList)

    def p_elseMarker(self, p):
        """ elseMarker :
        """
        self.quadList.append(genQuad('', '', 'goto'))
        p[0] = Attributes()
        p[0].nextList.append(len(self.quadList) -  1)
        p[0].next = len(self.quadList)

    def p_selection_statement_3(self, p):
        """ selection_statement : SWITCH LPAREN expression RPAREN statement """
        p[0] = Attributes(); p[0].node = genNodes(["SWITCH",  p[3].node, p[5].node], self.dotFile)

    def p_iteration_statement_1(self, p):
        """ iteration_statement : WHILE LPAREN marker expression RPAREN whilemarker2 statement """
        self.quadList.append(genQuad('', p[3].quad, 'goto'))
        backpatch(self.quadList, p[7].breakList, len(self.quadList) + 1)
        backpatch(self.quadList, p[6].falseList, len(self.quadList) + 1)
        backpatch(self.quadList, p[7].continueList, p[3].quad)


        p[0] = Attributes(); 
        p[0].node = genNodes(["WHILE-LOOP", p[4].node, p[7].node], self.dotFile)

    def p_whilemarker2(self,p):
        """ whilemarker2 :
        """
        p[0] = Attributes()
        p[0].next = len(self.quadList) + 2
        self.quadList.append(genQuad( self.quadList[len(self.quadList) - 1][0], '', 'jfalse'))
        p[0].falseList.append(len(self.quadList) - 1)

    def p_iteration_statement_2(self, p):
        """ iteration_statement : DO do_while_marker statement WHILE LPAREN do_while_marker expression RPAREN SEMI """
        p[0] = Attributes(); 
        p[0].node = genNodes(["DO-WHILE-LOOP", p[3].node, p[7].node ], self.dotFile)
        self.quadList.append(genQuad( self.quadList[len(self.quadList) - 1][0], p[2].next , 'jtrue'))
        backpatch(self.quadList, p[3].breakList, len(self.quadList) + 1)
        backpatch(self.quadList, p[3].continueList, p[6].next)

    def p_do_while_marker(self, p):
        """ do_while_marker :
        """
        p[0] = Attributes()
        p[0].next = len(self.quadList) + 1

    

    def p_iteration_statement_3(self, p):
        """ iteration_statement : FOR LPAREN expression_opt SEMI formarker1 expression_opt SEMI formarker2 expression_opt RPAREN formarker3 statement """
        self.quadList.append(genQuad('', p[8].next, 'goto'))

        # concat(p[8].falseList, p[6].falseList) # changeT
        # backpatch(self.quadList, p[6].trueList, p[11].quad) # changeT

        backpatch(self.quadList, p[11].nextList, p[5].quad )
        backpatch(self.quadList, p[8].nextList,  p[11].quad )
        backpatch(self.quadList, p[12].continueList, p[8].next)
        backpatch(self.quadList, p[12].breakList, len(self.quadList) + 1)
        backpatch(self.quadList, p[8].falseList, len(self.quadList) + 1)
        self.for_flag -= 1
        p[0] = Attributes();
        p[0].node = genNodes(["FOR-LOOP", p[3].node, p[6].node,  p[9].node, p[12].node], self.dotFile)

    def p_formarker1(self, p):
        """ formarker1 :
        """
        p[0] = Marker()
        p[0].quad = len(self.quadList) + 1
        self.for_flag += 1

    def p_marker(self, p):
        """ marker :
        """
        p[0] = Marker()
        p[0].quad = len(self.quadList) + 1

    def p_formarker2(self, p):
        """ formarker2 :
        """
        p[0] = Attributes()
        p[0].next = len(self.quadList) + 3
        # self.quadList.append(genQuad( self.quadList[len(self.quadList) - 1][0], '', 'jfalse'))
        self.quadList.append(genQuad( self.quadList[len(self.quadList) - 3][0], '', 'jfalse') ) #changeT
        p[0].falseList.append(len(self.quadList) - 1)
        self.quadList.append(genQuad('', '', 'goto'))
        p[0].nextList.append(len(self.quadList) - 1)

    def p_formarker3(self, p):
        """ formarker3 :
        """
        p[0] = Marker()
        p[0].quad = len(self.quadList) + 2
        self.quadList.append(genQuad('', '', 'goto'))
        p[0].nextList.append(len(self.quadList) - 1)

    def p_iteration_statement_4(self, p):
        """ iteration_statement : FOR LPAREN declaration expression_opt SEMI expression_opt RPAREN statement """
        p[0] = Attributes(); p[0].node = genNodes(["FOR-LOOP", p[3].node, p[4].node,  p[6].node.node, p[8].node], self.dotFile)


    def p_jump_statement_1(self, p):
        """ jump_statement  : GOTO ID SEMI """
	p[0] = Attributes();
        p[0].node = genNodes(["jump_statement",[p[1]],[p[2]],[p[3]]], self.dotFile)

    def p_jump_statement_2(self, p):
        """ jump_statement  : BREAK SEMI """
	p[0] = Attributes();
        self.quadList.append(genQuad('', '', 'goto'))
        p[0].breakList.append(len(self.quadList) - 1 )
        p[0].node = genNodes(["jump_statement",[p[1]], [p[2]]], self.dotFile)

    def p_jump_statement_3(self, p):
        """ jump_statement  : CONTINUE SEMI """
        p[0] = Attributes();
        self.quadList.append(genQuad('', '', 'goto'))
        p[0].continueList.append(len(self.quadList) - 1)
        p[0].node = genNodes(["jump_statement",[p[1]]], self.dotFile)

    def p_jump_statement_4(self, p):
        """ jump_statement  : RETURN expression SEMI
                            | RETURN SEMI
        """
	if len(p) == 4:
		p[0] = Attributes();
                p[0].node = genNodes(["jump_statement",[p[1]],p[2].node], self.dotFile)
                self.quadList.append(genQuad( p[2].value, '', 'retq_value'))
	else:
		p[0] = Attributes();
                p[0].node = genNodes(["jump_statement",[p[1]]], self.dotFile)
                self.quadList.append(genQuad('','','retq_null'))

    def p_expression_statement(self, p):
        """ expression_statement : expression_opt SEMI """
        p[0] = Attributes();
        p[0].node = p[1].node

    def p_expression(self, p):
        """ expression  : assignment_expression
                        | expression COMMA assignment_expression
        """
        if len(p) == 2:
                p[0] = Expression();
                p[0].node = p[1].node
                p[0].value = p[1].value
                p[0].falseList = p[1].falseList
                p[0].trueList = p[1].trueList
        else:
        	p[0] = Attributes(); 
                p[0].node = genNodes(["expression",p[1].node,[p[2]],p[3].node], self.dotFile)
                p[1].value = None
	
    def p_typedef_name(self, p):
        """ typedef_name : TYPEID """
        p[0] = Attributes();
        p[0].node = genNodes(["typedef_name",[p[1]]], self.dotFile)

    def p_assignment_expression(self, p):
        """ assignment_expression   : conditional_expression
                                    | unary_expression assignment_operator assignment_expression
        """
        if len(p) == 2:
                p[0] = Expression();
                p[0].node = p[1].node
                p[0].value = p[1].value
                p[0].falseList = p[1].falseList
                p[0].trueList = p[1].trueList
        else:
                tmpRHS = self.astStack.pop()
                tmpASSIGN = self.astStack.pop()
                tmpLHS = self.astStack.pop()

                if tmpLHS[1] != tmpRHS[1]:
                    p[0] = Expression()
                    p[0].value = True
                    if tmpLHS[1] == 'int' and tmpRHS[1] == 'float':
                        self._warning(self.clex.lexer.lineno, "Implicit conversion from float to int")
                        p[0].node = genNodes([tmpASSIGN + " (float to int)",p[1].node,p[3].node], self.dotFile)
                        tmpASSIGN = tmpASSIGN + "f"
                    elif tmpLHS[1] == 'float' and tmpRHS[1] == 'int':
                        p[0].node = genNodes([tmpASSIGN + " (int to float)",p[1].node,p[3].node], self.dotFile)
                        tmpASSIGN = tmpASSIGN + "f"
                    elif tmpLHS[1] == 'char' and tmpRHS[1] == 'int':
                        self._warning(self.clex.lexer.lineno, "Implicit conversion from int to char")
                        p[0].node = genNodes([tmpASSIGN + " (char to int)",p[1].node,p[3].node], self.dotFile)
                        tmpASSIGN = tmpASSIGN + "i"
                    elif tmpLHS[1] == 'int' and tmpRHS[1] == 'char':
                        self._warning(self.clex.lexer.lineno, "Implicit conversion from char to int")
                        p[0].node = genNodes([tmpASSIGN + " (char to int)",p[1].node,p[3].node], self.dotFile)
                        tmpASSIGN = tmpASSIGN + "i"
                    elif tmpRHS[0] == None and isinstance(tmpRHS[1], tuple) and len(tmpRHS[1]) >= 2 and len(tmpRHS[1]) >=2 and isinstance(tmpRHS[1][1], list) and tmpRHS[1][0] == tmpLHS[1]:
                        p[0].node = genNodes([tmpASSIGN ,p[1].node,p[3].node], self.dotFile)
                        tmpASSIGN = tmpASSIGN
                    else:
                        self._raise_error(self.clex.lexer.lineno, "Type mismatch: Cannot convert " + str(tmpRHS[1]) + " to " + str(tmpLHS[1]))
                        p[0].node = genNodes([tmpASSIGN + "\nERROR: TYPE MISMATCH",p[1].node,p[3].node], self.dotFile)
                        p[0].value = False
                else:
                        p[0] = Expression()
                        if isinstance(tmpLHS[1], tuple) and tmpLHS[1][0] == 'pointer':
                            tmpLHS = list(tmpLHS)
                            tmpLHS[1] = 'pointer'
                        p[0].node = genNodes([tmpASSIGN + tmpLHS[1],p[1].node,p[3].node], self.dotFile)
                        p[0].value = True
                        if tmpLHS[1] == 'int':
                            tmpASSIGN += 'i'
                        if tmpLHS[1] == 'char':
                            tmpASSIGN += 'c'
                        if tmpLHS[1] == 'float':
                            tmpASSIGN += 'f'
                        if tmpLHS[1] == 'pointer':
                            tmpASSIGN += 'p'

                t1 =self.evalStack.pop()
                t2 =self.evalStack.pop()

                if len(p[1].falseList) != 0:
                    # backpatch(self.quadList, p[1].falseList, len(self.quadList))
                    self.quadList.append(genQuad( t1, "", tmpASSIGN, t2))
                
                # if isinstance(t1, str) and len(t1) >=1 and t1[0] == '*':
                    # t1=t1[1:]

                val1 = self.scopeStack.search(t1)
                val2 = self.scopeStack.search(t2)


                if isinstance(val1, list):
                    val1 = val1[0] # changed from self.scopeStack.changeValue(t1, t2)
                elif val1 == False:
                    try:
                        val1 = int(t1)
                    except:
                        try:
                            val1 = float(t1)
                        except:
                            pass
                            # print("Warning (Ignore): Internal compiler error: " + t1 + " is an identifier but its entry in symbol table is not a list." )

                ###########################################################
                self.quadList.append(genQuad( t1, "", tmpASSIGN, t2))
                self.scopeStack.changeValue(None,t2)
                self.scopeStack.changeValue(t1, t2)
                ###########################################################

                # if isinstance(val2, list):
                    # val2 = val2[0] # changed from self.scopeStack.changeValue(t1, t2)

                # if self.for_flag == 0:
                    # op = tmpASSIGN
                    # if op in ['=i', '=f', '=c', '=p']:
                        # if not isinstance(val1, bool) and type(val1) in ['int', 'float', 'char']:
                            # self.scopeStack.changeValue(str(val1), t2)
                            # self.quadList.append(genQuad( str(val1), "", "=", t2)) #changeU
                        # else:
                            # self.quadList.append(genQuad( t1, "", tmpASSIGN, t2))
                    # else:
                        # op = op[0]
                        # if val1 and val2 and type(val1) in ['int', 'float', 'char'] and  type(val2) in ['int', 'float', 'char']:
                            # tmpval = eval(val2 + op + str(val1))
                            # self.scopeStack.changeValue(str(tmpval),t2)
                            # self.quadList.append(genQuad( str(tmpval), "", "=", t2)) #changeU
                        # else:
                            # self.quadList.append(genQuad( t1, "", tmpASSIGN, t2))
                # else:
                    # self.quadList.append(genQuad( t1, "", tmpASSIGN, t2))
                    # self.scopeStack.changeValue(None,t2)

    


    def p_assignment_operator(self, p):
        """ assignment_operator : EQUALS
                                | XOREQUAL
                                | TIMESEQUAL
                                | DIVEQUAL
                                | MODEQUAL
                                | PLUSEQUAL
                                | MINUSEQUAL
                                | LSHIFTEQUAL
                                | RSHIFTEQUAL
                                | ANDEQUAL
                                | OREQUAL
        """
        self.astStack.push(p[1])
	# p[0] = Attributes(); p[0].node = genNodes(["assignment_operator",[p[1].node]], self.dotFile)
	#p[0] = Attributes(); p[0].node = p[1].node

    def p_constant_expression(self, p):
        """ constant_expression : conditional_expression """
	p[0] = Attributes(); p[0].node = genNodes(["constant_expression",p[1].node], self.dotFile)
	#p[0] = Attributes(); p[0].node = p[1].node

    def p_conditional_expression(self, p):
        """ conditional_expression  : binary_expression
                                    | binary_expression CONDOP expression COLON conditional_expression
        """
        if len(p) == 2:
                p[0] = Expression()
                p[0].node = p[1].node
                p[0].value = p[1].value
                p[0].falseList = p[1].falseList
                p[0].trueList = p[1].trueList
        else:
		p[0] = Attributes()
                p[0].node = genNodes(["conditional_expression",p[1].node, [p[2].node],p[3].node,[p[4].node], p[5].node], self.dotFile)

    def p_binary_expression_1(self, p):
        """ binary_expression   : cast_expression"""
        p[0] = Expression()
        p[0].node = p[1].node
        p[0].value = p[1].value

    def p_binary_expression_2(self, p):
        """ binary_expression   : binary_expression TIMES binary_expression
                                | binary_expression DIVIDE binary_expression
                                | binary_expression MOD binary_expression
                                | binary_expression PLUS binary_expression
                                | binary_expression MINUS binary_expression
                                | binary_expression RSHIFT binary_expression
                                | binary_expression LSHIFT binary_expression
        """
        p[0] = Expression()
        p[0].value = self.astForBinaryExpr(p)

    def p_binary_expression_3(self, p):
        """ binary_expression   : binary_expression LT binary_expression
                                | binary_expression LE binary_expression
                                | binary_expression GE binary_expression
                                | binary_expression GT binary_expression
                                | binary_expression EQ binary_expression
                                | binary_expression NE binary_expression
        """
        p[0] = Expression()
        prevInstrRes = self.astForBinaryExpr(p)
        self.quadList.append( genQuad( prevInstrRes, '', 'jtrue' ) )
        p[0].trueList.append( len(self.quadList) - 1 )
        self.quadList.append( genQuad( prevInstrRes, '', 'jfalse' ) )
        p[0].falseList.append( len(self.quadList) - 1 )

    def p_binary_expression_AND(self, p):
        """ binary_expression   : binary_expression AND marker binary_expression
        """
        p[0] = Expression()
        quad = self.astForBinaryExpr(p)

    def p_binary_expression_OR(self, p):
        """ binary_expression   : binary_expression OR binary_expression
        """
        p[0] = Expression()
        self.astForBinaryExpr( p)

    def p_binary_expression_XOR(self, p):
        """ binary_expression   : binary_expression XOR binary_expression
        """
        p[0] = Expression()
        self.astForBinaryExpr( p)

    def p_binary_expression_LAND(self, p):
        """ binary_expression   : binary_expression LAND marker binary_expression
        """
        p[0] = Expression()
        quad = self.astForBinaryExpr(p)
        backpatch( self.quadList, p[1].trueList, p[3].quad )
        p[0].trueList = p[4].trueList
        p[0].falseList = concat( p[1].falseList, p[4].falseList , 1)

    def p_binary_expression_LOR(self, p):
        """ binary_expression   : binary_expression LOR marker binary_expression
        """
        p[0] = Expression()
        quad = self.astForBinaryExpr( p)
        backpatch( self.quadList, p[1].falseList, p[3].quad )
        p[0].trueList = concat(p[1].trueList, p[4].trueList, 1)
        p[0].falseList = p[4].falseList

    def p_cast_expression_1(self, p):
        """ cast_expression : unary_expression """
        p[0] = Expression(); 
        p[0].node = p[1].node
        p[0].value = p[1].value

    def p_cast_expression_2(self, p):
        """ cast_expression : LPAREN type_name RPAREN cast_expression """
	p[0] = Attributes();
        p[0].node = genNodes(["cast_expression",[p[1].node],p[2].node,[p[3].node],p[4].node], self.dotFile)

    def p_unary_expression_1(self, p):
        """ unary_expression    : postfix_expression """
        #Handle calls to arrays
        RefArray= []
        if self.evalStack.top() == 'array_reference_end':
            self.evalStack.pop()
            tmp = self.evalStack.pop()
            while tmp != 'array_reference_begin':
                RefArray.append(tmp)
                tmp = self.evalStack.pop()

            tmpid = self.evalStack.top()
            ActArray = self.scopeStack.search(tmpid)
            if not (isinstance(ActArray[1], tuple) and len(ActArray[1]) >= 3 and ActArray[1][1] == 'array'):
                self._raise_error(self.clex.lexer.lineno, tmpid + " is not an array, cannot be indexed")

            ActArraySize = ActArray[1][2]
            ActArrayType = ActArray[1][0]
            RefArray.reverse()
            if not (len(RefArray) == len(ActArraySize)):
                self._raise_error(self.clex.lexer.lineno, "Incorrect number of dimensions for array " + tmpid + str(ActArraySize))

            for i in range(0, len(RefArray)):
                try:
                    i1 = int(RefArray[i])
                    i2 = int (ActArraySize[i])
                except:
                    i1 = RefArray[i]

                if isinstance(i1, int) and i1 < 0:
                    self._raise_error(self.clex.lexer.lineno, "Array indices must be non-negative")
                if isinstance(i1, int) and i1 > i2:
                    self._raise_error(self.clex.lexer.lineno, "Array index out of bounds")

            arrayLen = len(ActArraySize)
            tmpquad = genQuad(RefArray[0], "", "=")
            self.quadList.append(tmpquad)
            self.scopeStack.addtoTop([RefArray[0],'int'], tmpquad[0])

            for i in range(1, arrayLen):
                tmpquad = genQuad(tmpquad[0], ActArraySize[i], "*")
                self.scopeStack.addtoTop([tmpquad[1] +tmpquad[3] + tmpquad[2],'int'], tmpquad[0])
                self.quadList.append(tmpquad)

                tmpquad = genQuad(tmpquad[0], RefArray[i], "+")
                self.scopeStack.addtoTop([tmpquad[1] +tmpquad[3] + tmpquad[2],'int'], tmpquad[0])
                self.quadList.append(tmpquad)

            tmpquad = genQuad(tmpquad[0], returnWidth(ActArrayType), "*" )
            self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]) + str(tmpquad[2]),'int'], tmpquad[0])
            self.quadList.append(tmpquad)

            #remove identifier name from evalStack
            tmpid = self.evalStack.pop()

            tmpquad = genQuad(tmpid, tmpquad[0], "[]=" )
            self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]) + str(tmpquad[2]),'int'], tmpquad[0])
            tmpquad[0] = "*" + tmpquad[0]
            self.quadList.append(tmpquad)

            #add tmp variable to evalStack
            self.evalStack.push(tmpquad[0])

        p[0] = Expression();
        p[0].node = p[1].node
        try:
            p[0].value = p[1].value
        except:
            print(bcolors.OKGREEN + "unary_expression_1: p[1] is an instance of Attributes(); DOES NOT have attribute value" + bcolors.ENDC)

    def p_unary_expression_2(self, p):
        """ unary_expression    : PLUSPLUS unary_expression
                                | MINUSMINUS unary_expression
        """
	p[0] = Attributes();
        p[0].node = genNodes(["unary_expression", [p[1].node], p[2].node], self.dotFile)

    def p_unary_expression_3(self, p):
        """ unary_expression    : SIZEOF unary_expression
                                | SIZEOF LPAREN type_name RPAREN
        """
	if len(p) == 3:
		p[0] = Attributes(); p[0].node = genNodes(["unary_expression",[p[1].node],p[2].node], self.dotFile)
	else:
		p[0] = Attributes(); p[0].node = genNodes(["unary_expression",[p[1].node],[p[2].node],p[3].node,[p[4].node]], self.dotFile)
	
    def p_unary_expression_4(self, p):
        """ unary_expression : unary_operator cast_expression
        """
        t1 = self.astStack.pop()
        t2 = self.astStack.pop()

        tmpid = self.evalStack.pop()
        tmpquad = genQuad(tmpid, "", t2)
        self.quadList.append(tmpquad)
        prevInstrRes = tmpquad[0]

        self.evalStack.push(prevInstrRes)
        if isinstance(t1, tuple):
            t1o = t1[1]
        else:
            t1o = t1
        t2o = t2

        exprtype = t1
        if t2o == '~':
            if t1o in ["char", "int"]:
                p[0] = Expression();
                p[0].node = genNodes(["unary_expression", [p[1].node + "(" + t1o + ")"], p[2].node], self.dotFile)
                p[0].value = prevInstrRes
                self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]), t1o], tmpquad[0])
            else:
                self._raise_error(self.clex.lexer.lineno, "Invalid operand " + t1o +  " for operator " + t2o )
        elif t2o == "*":
            if isinstance(t1o, tuple) and t1o[0] == ("pointer"):
                p[0] = Expression()
                p[0].node = genNodes(["unary_expression", [p[1].node +"(pointer)"], p[2].node], self.dotFile)
                p[0].value = prevInstrRes
                self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]), str(p[1].node) + "(pointer)"], tmpquad[0])
            else:
                self._raise_error(self.clex.lexer.lineno, "Invalid operand " + t1o +  " for operator " + t2o )
        elif t2o == "&":
            if t1[2] == "identifier" or 'identifier_array':
                p[0] = Expression()
                p[0].node = genNodes([p[1].node+"pointer", p[2].node], self.dotFile)
                p[0].value = prevInstrRes
                self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]), ("pointer", str(self.scopeStack.search(tmpid)[1]))], tmpquad[0])
                exprtype = (str(tmpquad[1]) + str(tmpquad[3]), ("pointer", self.scopeStack.search(tmpid)[1]), 'identifier')
            else:
                self._raise_error(self.clex.lexer.lineno, "Invalid operand " + t1o +  " for operator " + t2o )
        else:
            if t1o in ["float", "int", "char"]:
                p[0] = Expression()
                p[0].node = genNodes(["unary_expression", [p[1].node + "(" + t1o + ")"], p[2].node], self.dotFile)
                p[0].value = prevInstrRes
                self.scopeStack.addtoTop([str(tmpquad[1]) + str(tmpquad[3]), t1o], tmpquad[0])

                if t2o == '!':
                    p[0].trueList = p[2].falseList
                    p[0].falseList= p[2].trueList
            else:
                self._raise_error(self.clex.lexer.lineno, "Invalid operand " + t1o +  " for operator " + t2o )

        self.astStack.push(exprtype)

    def p_unary_operator(self, p):
        """ unary_operator  : AND
                            | TIMES
                            | PLUS
                            | MINUS
                            | NOT
                            | LNOT
        """
        self.astStack.push(p[1])
        p[0] = Attributes()
        p[0].node = genNodes([p[1]],self.dotFile)
        
    def p_postfix_expression_1(self, p):
        """ postfix_expression  : primary_expression """
        p[0] = Expression();
        p[0].node = p[1].node
        p[0].value = p[1].value

    def p_postfix_expression_2(self, p):
        """ postfix_expression  : postfix_expression LBRACKET expression RBRACKET """
	p[0] = Expression()
        p[0].node = genNodes(["postfix_expression",p[1].node,[p[2]],p[3].node,[p[4]]], self.dotFile)
        p[0].value = p[1].value

        #Hoping that this handles only array elements' assignments e.g. a[3][1] = 3
        t1 = self.evalStack.pop()
        t2 = self.evalStack.pop()
        if t2 == 'array_reference_end':
            self.evalStack.push(t1)
        else:
            self.evalStack.push(t2)
            self.evalStack.push("array_reference_begin")
            self.evalStack.push(t1)
        self.evalStack.push("array_reference_end")

        self.astStack.pop()
        t3 = self.astStack.pop()
        if isinstance(t3[1], tuple) and t3[1][1] == 'array':
            self.astStack.push((None, t3[1][0], 'identifier_array'))
        else:
            self.astStack.push(t3)

    def p_postfix_expression_3(self, p):
        """ postfix_expression  : postfix_expression LPAREN argument_expression_list RPAREN
                                | postfix_expression LPAREN RPAREN
        """
	if len(p) == 5:
                argsVar = [] #Variable names/ values of the arguments passed
                for i in range(0, self.no_of_arg.pop()):
                    param = self.evalStack.pop()
                    if type(param) in ['int', 'float', 'char']:
                        self.quadList.append(genQuad(param, '', 'push')) # generate quadruple to push parameter on stack
                    else:
                        paramType = self.scopeStack.search(param)
                        if type(paramType[0]) in ['int', 'float', 'char']:
                            self.quadList.append(genQuad(paramType[0], '', 'push')) # generate quadruple to push parameter on stack
                        else:
                            self.quadList.append(genQuad(param, '', 'push')) # generate quadruple to push parameter on stack

                    argsVar.append(param)
                self.no_of_arg.pop()
                argsVar.reverse()

                tmpstr = self.evalStack.pop() 
                FUNCname = tmpstr
                tmpstr += "("
                for i in range(0, len(argsVar)):
                    tmpstr += argsVar[i]
                    tmpstr += ","
                tmpstr += ")"

                self.evalStack.push(tmpstr)

                ARGS = self.astStack.pop() # Passed arguments
                FUNC = self.astStack.top()

                FUNCreturnType = FUNC[1][0]
                FUNCargs = FUNC[1][1]

                FUNCcall = list(self.evalStack.top())
                del FUNCcall[-2]
                FUNCcall = ''.join(FUNCcall)

                #check if FUNC is a funtion
                if not(isinstance(FUNC, tuple) and isinstance(FUNC[1], tuple) and FUNC[0] == None and isinstance(FUNC[1][1], list)):
                    self._raise_error(self.clex.lexer.lineno, FUNCcall + " : NOT A FUNCTION")

                if len(FUNCargs) != len(ARGS):
                    self._raise_error(self.clex.lexer.lineno, FUNCcall + ": Function requires " + str(len(FUNCargs)) + " arguments but " + str(len(ARGS)) + " provided")
                else:
                    for i in range(0,len(FUNCargs)):
                        if FUNCargs[i] != ARGS[i][1]:
                            errorMsg =  FUNCcall + ": Incompatible argument type " + str(i+1) + ": Required " + str(FUNCargs[i]) + " but " + str(ARGS[i][1]) + " provided"
                            self._raise_error(self.clex.lexer.lineno, errorMsg)
                
                tmp = ["Function Call \n (postfix_exp_3)", p[1].node]
                tmp.extend(p[3].node)
                p[0] = Expression()
                p[0].node = genNodes(["postfix_expression\n( FUNC call )", p[1].node, p[3].node] , self.dotFile)
                p[0].value = p[1].value
                self.quadList.append(genQuad( FUNCname, '', 'call'))
	else:
                FUNC = self.astStack.top()
                FUNCreturnType = FUNC[1][0]
                FUNCargs = FUNC[1][1]
                FUNCname = self.evalStack.top()

                if len(FUNCargs) != 0:
                    self._raise_error(self.clex.lexer.lineno, FUNCname + ": Function requires " + str(len(FUNCargs)) + " arguments but ZERO provided")

		p[0] = Expression()
                p[0].node = genNodes(["postfix_expression",p[1].node,[p[2]],[p[3]]], self.dotFile)
                p[0].value = p[1].value
                self.quadList.append(genQuad( FUNCname, '', 'call'))

    def p_postfix_expression_4(self, p):
        """ postfix_expression  : postfix_expression PERIOD ID
                                | postfix_expression PERIOD TYPEID
                                | postfix_expression ARROW ID
                                | postfix_expression ARROW TYPEID
        """
        if p[2] == '.':
            tmp_struct_obj = self.evalStack.pop()
            tmp = self.scopeStack.search(tmp_struct_obj)
            
            if not tmp:
                self._raise_error(self.clex.lexer.lineno, "Undeclared_1 variable " + str(tmp_struct_obj), errorType = 'undeclared', var = str(tmp_struct_obj))
            elif tmp[1][0] != 'struct':
                self._raise_error(self.clex.lexer.lineno, str(tmp_struct_obj) + " of type " + str(tmp[1][0]) + " cannot reference a variable. Struct required.")
            else:
                tmp_struct = self.scopeStack.search(tmp[1][1])
                if not tmp_struct:
                    self._raise_error(self.clex.lexer.lineno, "Undeclared_1 variable " + str(tmp[1][1]), errorType = 'undeclared', var = str(tmp_struct_obj))

                tmp_struct = tmp_struct[0]
                if p[3] not in tmp_struct:
                    self._raise_error(self.clex.lexer.lineno, p[3] + " is not defined in the scope of " + tmp[1][1])
                else:
                    tmp = (tmp_struct[p[3]][0], tmp_struct[p[3]][1], "identifier")
                    self.astStack.pushpop(tmp)
                    self.evalStack.push(str(tmp_struct_obj) + "." + str(p[3]))

        else:
            waste_time = 1
	p[0] = Expression()
        p[0].node = genNodes(["postfix_expression", p[1].node, [p[2]],[p[3]]], self.dotFile)

    def p_postfix_expression_5(self, p):
        """ postfix_expression  : postfix_expression PLUSPLUS
                                | postfix_expression MINUSMINUS
        """
        tmpquad = genQuad(p[1].value, '', p[2] )
        self.scopeStack.addtoTop(None, tmpquad[0])
	p[0] = Attributes(); p[0].node = genNodes(["postfix_expression", p[1].node,[p[2]]], self.dotFile)

    def p_postfix_expression_6(self, p):
        """ postfix_expression  : LPAREN type_name RPAREN brace_open initializer_list brace_close
                                | LPAREN type_name RPAREN brace_open initializer_list COMMA brace_close
        """
	p[0] = Attributes(); p[0].node = genNodes(["postfix_expression", [p[1].node], p[2].node, [p[3].node],p[4].node , p[5].node, p[6].node.node, p[7].node], self.dotFile)

    def p_primary_expression_1(self, p):
        """ primary_expression  : identifier """
        p[0] = Expression()
        p[0].node = p[1].node
        p[0].value = p[1].value

    def p_primary_expression_2(self, p):
        """ primary_expression  : constant """
        p[0] = Expression()
        p[0].node = p[1].node
        p[0].value = p[1].value

    def p_primary_expression_3(self, p):
        """ primary_expression  : unified_string_literal
                                | unified_wstring_literal
        """
        p[0] = Attributes(); p[0].node = p[1].node

    def p_primary_expression_4(self, p):
        """ primary_expression  : LPAREN expression RPAREN """
        p[0] = Expression()
        p[0].node = p[2].node
        p[0].truelist = p[2].trueList
        p[0].falelist = p[2].falseList

    def p_primary_expression_5(self, p):
        """ primary_expression  : OFFSETOF LPAREN type_name COMMA offsetof_member_designator RPAREN
        """
	p[0] = Attributes(); p[0].node = genNodes(["p_primary_expression",[p[1].node],[p[2].node],p[3].node,[p[4].node],p[5].node,[p[6].node.node]], self.dotFile)

    def p_offsetof_member_designator(self, p):
        """ offsetof_member_designator : identifier
                                         | offsetof_member_designator PERIOD identifier
                                         | offsetof_member_designator LBRACKET expression RBRACKET
        """
        if len(p) == 2:
            p[0] = Attributes(); p[0].node = genNodes(["offsetof_member_designator",p[1].node], self.dotFile)
        elif len(p) == 4:
            p[0] = Attributes(); p[0].node = genNodes(["offsetof_member_designator",p[1].node,[p[2].node],p[3].node], self.dotFile)
        elif len(p) == 5:
            p[0] = Attributes(); p[0].node = genNodes(["offsetof_member_designator",p[1].node,[p[2].node],p[3].node,[p[4].node]], self.dotFile)
        else:
            raise NotImplementedError("Unexpected parsing state. len(p): %u" % len(p))

    def p_argument_expression_list(self, p):
        """ argument_expression_list    : assignment_expression
                                        | argument_expression_list COMMA assignment_expression
        """
        if len(p) == 2: # single expr
            self.no_of_arg.push("_")
            self.no_of_arg.push(1)
            self.astStack.push([self.astStack.pop()])
            p[0] = Attributes(); p[0].node = [p[1].node]
        else:
            t1 = self.astStack.pop()
            t2 = self.astStack.pop()
            t2.append(t1)
            self.astStack.push(t2)
            self.no_of_arg.pushpop( self.no_of_arg.top() + 1 )
            if isinstance(p[1].node, list):
                p[1].node.append(p[3].node)
                p[0] = Attributes(); p[0].node = p[1].node

    def p_identifier(self, p):
        """ identifier  : ID """
        tmpISARRAY = 0
        if not self.evalStack.empty() and  self.evalStack.top() == 'ISARRAY':
            tmpISARRAY = 1
            self.evalStack.pop()

        check = 0
        if not self.evalStack.empty():
            tmp = self.evalStack.top()
            if tmp == ("*", "pointer"):
                self.evalStack.pop()
                check = 1

        tmp = self.scopeStack.search(p[1])
        if not tmp and tmp != None:
            errorMsg = "Undeclared_2 variable " + p[1] + " referenced before assignment"
            self._raise_error(self.clex.lexer.lineno, errorMsg, errorType = 'undeclared', var = str(p[1]))
            self.astStack.push("UNDECLARED")
            p[0] = Expression()
            p[0].node = genNodes(["identifier (UNDECLARED ERROR)", [p[1]]], self.dotFile)
        else:
            self.evalStack.push(p[1])
            if check == 0:
                self.astStack.push((tmp[0], tmp[1], "identifier"))
            else:
                self.astStack.push((tmp[0], tmp[1], "pointer"))
            if tmpISARRAY != 1:
                p[0] = Expression()
                p[0].node = genNodes([p[1] + "\n(" + str(tmp[1]) + ")"], self.dotFile)
                p[0].value = self.scopeStack.search(p[1])[0]

    def p_constant_1(self, p):
        """ constant    : INT_CONST_DEC
                        | INT_CONST_OCT
                        | INT_CONST_HEX
                        | INT_CONST_BIN
        """
        if self.evalStack.size() > 1 and self.evalStack.top() != 'ISARRAY':
            t1 = self.evalStack.pop()
            t2 = self.evalStack.top()
            self.evalStack.push(t1)
            if isinstance(t2, tuple) and t2[0] == 'array':
                p[0] = Attributes(); p[0].node = p[1]
            else:
                p[0] = Expression()
                p[0].node = genNodes([str(p[1]) + "\n(int const)"], self.dotFile)
                p[0].value = p[1]
        else:
            self.evalStack.pop()
            p[0] = Expression
            p[0].node = p[1]
            p[0].value = p[1]

        self.evalStack.push(p[1])
        self.astStack.push((p[1], 'int', "constant"))

    def p_constant_2(self, p):
        """ constant    : FLOAT_CONST
                        | HEX_FLOAT_CONST
        """
        p[0] = Expression()
        if len(self.evalStack.stack) > 1 and self.evalStack.top() != 'ISARRAY':
            t1 = self.evalStack.pop()
            t2 = self.evalStack.top()
            self.evalStack.push(t1)
            if isinstance(t2, tuple) and t2[0] == 'array':
                p[0].node = p[1]
            else:
                p[0].node = genNodes([str(p[1]) + "\n(float const)"], self.dotFile)
        else:
            self.evalStack.pop()
            p[0].node = p[1]
        p[0].value = p[1]
        self.evalStack.push(p[1])
        self.astStack.push((p[1], "float", "constant"))


    def p_constant_3(self, p):
        """ constant    : CHAR_CONST
                        | WCHAR_CONST
        """
        p[0] = Expression()
        if self.evalStack.top() != 'ISARRAY':
            t1 = self.evalStack.pop()
            t2 = self.evalStack.top()
            self.evalStack.push(t1)
            if isinstance(t2, tuple) and t2[0] == 'array':
                p[0].node = p[1]
            else:
                p[0].node = genNodes([str(p[1]) + "\n(float const)"], self.dotFile)
        else:
            self.evalStack.pop()
            p[0].node = p[1]

        p[0].value =p[1]

        self.evalStack.push(p[1])
        self.astStack.push((p[1], "char", "constant"))

    ### The "unified" string and wstring literal rules are for supporting
    ### concatenation of adjacent string literals.
    ### I.e. "hello " "world" is seen by the C compiler as a single string literal
    ### with the value "hello world"
    ###
    def p_unified_string_literal(self, p):
        """ unified_string_literal  : STRING_LITERAL
                                    | unified_string_literal STRING_LITERAL
        """
        if len(p) == 2: # single literal
	    p[0] = Attributes(); p[0].node = genNodes(["unified_wstring_literal",[p[1]]], self.dotFile)
        else:
	    p[0] = Attributes(); p[0].node = genNodes(["unified_wstring_literal",p[0],[p[1]]], self.dotFile)

    def p_unified_wstring_literal(self, p):
        """ unified_wstring_literal : WSTRING_LITERAL
                                    | unified_wstring_literal WSTRING_LITERAL
        """

	if len(p) == 2: # single literal
            p[0] = Attributes(); p[0].node = c_ast.Constant(
                'string', p[1].node, self._coord(p.lineno(1)))
        else:
            p[1].node.value = p[1].node.value.rstrip()[:-1] + p[2].node[2:]
            p[0] = Attributes(); p[0].node = p[1].node

    def p_brace_open(self, p):
        """ brace_open  :   LBRACE
        """
        p[0] = None

    def p_brace_close(self, p):
        """ brace_close :   RBRACE
        """
        scope_dict = self.scopeStack.pop()
        scopeName = self.scopeNames.pop()
        self.quadList.append(genQuad(scopeName, '', 'scope_exit'))
        self.scopeTree.addData(scope_dict)#CHANGES
        self.scopeTree.changeCurrent(self.scopeTree.parent())#changeS
        p[0] = Attributes(); p[0].node = p[1]

    def p_empty(self, p):
        'empty : '
        #p[0] = Attributes(); p[0].node = None

    def p_error(self, p):
	    if p:
		 print(bcolors.WARNING + "Syntax error at token " + bcolors.FAIL + " " + p.value + bcolors.WARNING + "at lineno "+ bcolors.FAIL + str(p.lineno)  + bcolors.ENDC)
		 # Just discard the token and tell the parser it's okay.
                 self.cparser.errok()
	    else:
		 print(bcolors.WARNING + "Syntax error at" + bcolors.FAIL + " EOF" + bcolors.ENDC)

if __name__ == "__main__":
    import pprint
    import time, sys

