int is_odd (int );

int is_even(int n){
    if (n==0){
        return 1;
    }
    else{
        int m = n-1;
        int temp = is_odd(m);
        return temp;
    }
}

int is_odd(int n){
    if (n==0){
        return 0;
    }
    else{
        int m = n-1;
        int temp = is_even(m);
        return temp;
    }
}

int main(){
    int a = 5, b=4;
    a = is_odd(3047);
    printf(a);
    return;
}
