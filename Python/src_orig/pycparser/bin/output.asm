section .bss
count resb 4
indent resb 4


section .text
	mov ebp, esp
	global main
	extern printf
mov  eax, 0
mov [count], eax
mov  eax, 0
mov [indent], eax



_ackermann:
	push ebp
	mov ebp, esp; call function
	mov eax, [count]
	add eax, 1
	mov [ebp -44], eax
	mov  eax, [ebp -44]
	mov [count], eax
	mov eax, [ebp + 8]
	cmp eax, 0
	jge _L1
	_L1:
	mov eax, [ebp + 12]
	cmp eax, 0
	jge _L2
	mov eax, 0
	sub eax, 1
	mov [ebp -56], eax
	mov eax, [ebp -56]
	mov esp, ebp
	pop ebp
	ret
	_L2:
	mov eax, [ebp + 8]
	cmp eax, 0
	jnz _L3
	mov eax, [ebp + 12]
	add eax, 1
	mov [ebp -60], eax
	mov eax, [ebp -60]
	mov esp, ebp
	pop ebp
	ret
	_L3:
	mov eax, [ebp + 12]
	cmp eax, 0
	jnz _L4
	mov eax, [ebp + 8]
	sub eax, 1
	mov [ebp -64], eax
	push 1
	mov eax, [ebp -64]
	push eax
	call _ackermann
	mov [ebp -68], eax
	mov esp, ebp
	pop ebp
	ret
	_L4:
	mov eax, [ebp + 12]
	sub eax, 1
	mov [ebp -24], eax
	mov eax, [ebp -24]
	push eax
	mov eax, [ebp + 8]
	push eax
	call _ackermann
	mov [ebp -28], eax
	mov eax, [ebp + 8]
	sub eax, 1
	mov [ebp -8], eax
	mov eax, [ebp -28]
	push eax
	mov eax, [ebp -8]
	push eax
	call _ackermann
	mov [ebp -20], eax
	mov esp, ebp
	pop ebp
	ret
	mov esp, ebp
	pop ebp
	ret
main:
	push ebp
	mov ebp, esp; call function
	mov eax, 5
	push eax
	mov eax, 3
	push eax
	mov  eax, 3
	mov [ebp -12], eax
	mov  eax, 5
	mov [ebp -8], eax
	push 5
	push 3
	call _ackermann
	mov [ebp -16], eax
	mov eax, [ebp -16]
	push eax ;start print sequence
	push printf_int
	call printf
	add esp, 8 ; end print sequence
	mov esp, ebp
	pop ebp
	mov eax,4	;system call number (sys_write)
	int 0x80	;call kernel
	mov eax,1	;system call number (sys_exit)
	int 0x80	;call kernel


printf_int db "%d", 10, 0
printf_float db "%f", 10, 0
printf_char db "%c", 10, 0
