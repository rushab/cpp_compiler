section .data
const10: dd 10

section.text
    global _start

_start:
    mov eax, 345
    call printNumber
	mov eax,4	;system call number (sys_write)
	int 0x80	;call kernel
	mov eax,1	;system call number (sys_exit)
	int 0x80	;call kernel



printNumber:
    push eax
    push edx
    xor edx,edx          ;edx:eax = number
    div dword [const10]  ;eax = quotient, edx = remainder
    test eax,eax         ;Is quotient zero?
    je .l1               ; yes, don't display it
    call printNumber     ;Display the quotient
.l1:
    lea eax,[edx+'0']
    call printCharacter  ;Display the remainder
    pop edx
    pop eax
    ret

;printNumber:
    ;push ecx
    ;push edx
    ;mov ecx, eax
    ;mov edx, 1
    ;int 80h
    ;pop edx
    ;pop ecx 
    ;ret

printCharacter:
    push ecx
    push edx
    mov ecx, edx
    mov edx, 1
    int 80h
    pop edx
    pop ecx
    ret
