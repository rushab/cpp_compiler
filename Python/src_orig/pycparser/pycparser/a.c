struct A {
    int x;
    char y;
};

int main(){
    int i;
    char c;
    float f;
    struct A a;
    f = i; //No need to warn, no loss of data!
    i = f; //Need to warn, loss of data
    i = i + f; //check ast 
    c = i;

    f = c + a.y;
    f = c + f;
}
