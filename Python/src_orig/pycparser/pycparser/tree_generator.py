nodeNum = 0

def genNodes(n,dotFile): # n = Node list, l = Label list, l[i] = none
    global nodeNum
    #Create new node for n[0]
    nodeNum += 1
    rhsNode = "N" + str(nodeNum)
    dotFile.write(rhsNode+"-> {")
    
    newNodesStr = rhsNode +"[label = \"" + n[0] + "\"]\n"
    for i in range(1,len(n)):
        if type(n[i]) == list:
            nodeNum += 1 
            nodeName = "N" + str(nodeNum)
            dotFile.write(nodeName + " ")
            n[i][0] = n[i][0].replace("\"","")
            newNodesStr += nodeName + "[label = \"" + n[i][0] + "\"]\n"
        elif type(n[i]) == str:
            dotFile.write(n[i]+" ")
        else:
            print("Incorrect nodeList 'n' received: " + str(n[i]) + " of type <" + str(type(n[i])) + " at production for " + n[0])

    dotFile.write("}\n")
    dotFile.write(newNodesStr)
    return rhsNode 

