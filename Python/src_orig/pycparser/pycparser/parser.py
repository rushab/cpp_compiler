from c_parser import CParser
from lexer import *
from ply import yacc
import os
import csv
import sys
import argparse

parser = argparse.ArgumentParser(description = "Preprocess data")
parser.add_argument('file', type = str)
parser.add_argument('-AST', type = str, default = 'False')
args = parser.parse_args()

print(args.file)
codeText, lexer = lex(args.file)

lex_optimize  = False
yacc_optimize = False
yacc_debug    = False
taboutputdir  = ''

dotFile = open("../bin/graph.dot", "w")
dotFile.write("digraph G{\n")

symbolTableFile = open("../bin/symbolTables.csv", "w")

parser = CParser( dotFile = dotFile, symbolTableFile = symbolTableFile, lex_optimize=lex_optimize, lexer=CLexer, yacc_optimize = yacc_optimize, yacc_debug = yacc_debug, taboutputdir = taboutputdir)

parser.parse(codeText, args.file)

symbolTableFile.close()

dotFile.write("}")
dotFile.close()

if args.AST == 'True' or args.AST == '1':
    os.system("dot -Tps ../bin/graph.dot > ../bin/AST.ps && epstopdf ../bin/AST.ps > ../bin/AST.pdf && xdg-open ../bin/AST.pdf")
