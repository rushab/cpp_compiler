import re
import re
from collections import deque
from  functions import returnWidth, checkFunc, checkStruct

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

labelCnt = 0
numTabs = 0
def getTabs():
    tabs = ""
    for i in range(0, numTabs):
        tabs += '\t'
    return tabs

jumpInstr = {
        '<=i': ('jle', 'jg'),
        '<=f': ('jle', 'jg'),
        '>=i': ('jge', 'jl'),
        '>=f': ('jge', 'jl'),
        '<i' : ('jl', 'jge'),
        '<f' : ('jl', 'jge'),
        '>i' : ('jg', 'jle'),
        '>f' : ('jg', 'jle')
        }

class Function:
    def __init__(self, name):
        self.name = name
        self.label = None
        self.text = []
        self.scope = None
        self.returnType = None
        self.paramTypes = None
        self.offsets = {}
        self.offsetSum = 8
        self.sizeList = []
        self.paramList = []

    def addInstr(self, text):
        self.text.append(text)
    


class x86:
    def __init__(self):
        self.bss = ['section .bss']
        printf_int_label = 'printf_int db "%d", 10, 0'
        printf_char_label = 'printf_char db "%c", 10, 0'
        printf_float_label = 'printf_float db "%f", 10, 0'

        self.text = ['section .text',"\tmov ebp, esp", "\tglobal main", "\textern printf"]
        self.printf_labels = [printf_int_label, printf_float_label, printf_char_label]
        self.data = ['section .data']
        self.funcList = {}
        self.curFunc = 'global'
        self.sizeList = []
        self.structList = {}
        self.labelQueue = deque()
        self.labelPositions = deque()
        self.scopeTree = None
        self.quadList = None

    def create_bss(self, symTable, funcParamList):
        for i in symTable:
            if checkFunc(symTable[i]):
                self.newFunction(i, symTable[i][1], funcParamList[i])
            elif checkStruct(symTable[i]):
                self.newStruct(i, symTable[i][0])
            else:
                bssInstr = str(i) + " resb "  + str(returnWidth('int') )
                self.bss.append(bssInstr)

    def newFunction(self, fname, ftype, fparams):
        self.funcList[fname] = Function(fname)
        self.funcList[fname].returnType = ftype[0]
        self.funcList[fname].paramTypes = ftype[1]
        self.funcList[fname].paramList = fparams

    def newStruct(self, sname, sdecl):
        self.structList[sname] = sdecl

    def addInstr(self, instr):
        tabs = getTabs()
        if not isinstance(instr, str):
            instr[0] = tabs + instr[0]
        else:
            instr = tabs + instr
        if self.curFunc == 'global':
            self.text.append(instr)
        else:
            self.curFunc.addInstr(instr)
        if self.curFunc == 'global':
            lenCurFunc = len(self.text) - 1
        else:
            lenCurFunc = len(self.curFunc.text) - 1
        return self.curFunc, lenCurFunc
    
    def add_struct_to_act_record(self, func, varName, scopeName, prefix = ""):
        prefix = prefix + varName + "."
        structScope = self.scopeTree.searchScope(scopeName, isStruct = True)
        for var in structScope:
            i = structScope[var]
            if isinstance(i[1], tuple) and i[1][0] == 'struct':
                self.add_struct_to_act_record(func, i, prefix)
            elif isinstance(i[1], tuple) and i[1][0] == 'pointer':
                self.add_pointer_to_act_record(func, i, prefix)
            else:
                width = returnWidth(i[1])
                func.offsets[prefix+var] = func.offsetSum
                func.offsetSum += width
                ival = i[0]
                if type(ival) != 'id':
                    if func.offset > 0:
                        self.addInstr('mov [ebp +' + str(func.offsetSum) + '], ' + scope[i][0])
                    else:
                        self.addInstr('mov [ebp ' + str(func.offsetSum) + '], ' + scope[i][0])
                # self.addInstr('inc esp')
                

    def add_pointer_to_act_record(self, func, varName, prefix = ""):
            width = returnWidth('pointer')
            func.offsets[varName] = func.offsetSum
            func.offsetSum += width
            ival = func.scope[varName][0]
            if type(ival) != 'id':
                self.addInstr('mov [ebp' + '+' + str(func.offsetSum) + '], ' + scope[i][0])
            # self.addInstr('inc esp')


    def createActRecord(self, func, fparams, scope = None):
        if scope == None:
            func.offset = 8
            scope = func.scope

        for i in scope :
            if i in fparams:
                continue
            idtype = scope[i]
            if isinstance(idtype[1], tuple) and idtype[0] == None and len(idtype[1]) == 3 and idtype[1][1] == 'array':
                width = 0
                for dim in idtype[1][2]:
                    # for temporary in range(0,int(dim)):
                        # self.addInstr('push 0')
                    width += int(dim) * returnWidth(idtype[1][0])
                # width = returnWidth(idtype[1])
                func.offsets[i] = -func.offsetSum
                func.offsetSum += width

                
            elif isinstance(idtype[1], tuple) and idtype[1][0] == 'struct':
                self.add_struct_to_act_record(func, i, idtype[1][1])
            elif isinstance(idtype[1], tuple) and idtype[1][0] == 'pointer':
                self.add_pointer_to_act_record(func, i)
            else:
                width = returnWidth(idtype[1])
                func.offsets[i] = -func.offsetSum
                func.offsetSum += width
                # self.addInstr('inc esp')
                ival = scope[i][0]
                if type(ival) in ['int', 'float', 'char']:
                    # if func.offsets[i] > 0:
                        self.addInstr('mov eax, ' + scope[i][0] )
                        self.addInstr('push eax')
                    # else:
                        # self.addInstr('mov [ebp' + str(func.offsetSum) + '] ' + scope[i][0])
                # else:
                    # self.addInstr('push 0'  )

        negsum = 4 
        lenParams = len(fparams)
        for i in range(lenParams - 1, -1, -1):
            width = returnWidth(func.paramTypes[i])
            negsum += width
            func.offsets[ fparams[i] ] = negsum
            func.offsetSum += width
        i=1
            
    def writeAsm(self):
        output = open('../bin/output.asm', 'w')

        for i in self.bss:
            output.write(i + "\n")
        output.write("\n\n")


        for i in self.text:
            output.write(i + "\n")
        output.write("\n\n")


        for f in self.funcList:
            if f == 'main':
                self.funcList[f].text[0] = 'main:'
                self.funcList[f].text.pop()
                self.funcList[f].text.append("\tmov eax,4\t;system call number (sys_write)")
                self.funcList[f].text.append("\tint 0x80\t;call kernel")
                self.funcList[f].text.append("\tmov eax,1\t;system call number (sys_exit)")
                self.funcList[f].text.append("\tint 0x80\t;call kernel")
            for i in self.funcList[f].text:
                output.write(i + "\n")
        output.write("\n\n")

        for i in self.printf_labels:
            output.write(i + "\n")
        output.close()


def genAsm(scopeTree, quadList, funcParamList):
    # print("_________________-")
    # print(funcParamList)
    # print("_________________-")

    global numTabs
    asm = x86()
    code = asm.text
    globalScope = scopeTree.root.data
    asm.create_bss(globalScope, funcParamList)
    asm.scopeTree = scopeTree
    asm.quadList = quadList

    labelSet = set()
    for i in range(0, len(quadList)):
        if quadList[i][3] in ['goto', 'jtrue', 'jfalse'] and quadList[i][2] != '' and quadList[i][2] not in labelSet:
            asm.labelPositions.append((quadList[i][2], i, newLabel(asm, quadList[i][2]))) # (jump_addr, current_quad_number, _label)
            labelSet.add(quadList[i][2])

    j = 0
    while j < len(quadList):
        checkInsertLabel(asm, j)
        curQuad = quadList[j]
        res = curQuad[0]; op1 = curQuad[1]; op2 = curQuad[2]; op = curQuad[3]
        res, op1, op2 = getOperands([res, op1, op2], funcType = asm.curFunc)

        if op in ['+i', '+f']:
            if type(op1) == 'id':
                op1 = '[' + op1 +']'
            elif type(op1) == 'ret_val':
                op1 = 'eax'
            if type(op2) == 'id':
                op2 = '[' + op2 +']'
            elif type(op2) == 'ret_val':
                op2 = 'eax'

            asm.addInstr('mov eax, ' + op1 )
            if op == '+i':
                asm.addInstr('add eax, ' + op2 )
            else:
                asm.addInstr('fadd eax, ' + op2 )
            asm.addInstr('mov ' + res + ', eax')

        elif op in ['-', '-i', '-f']:
            if op2 == '': #unary minus
                asm.addInstr('mov eax, 0')
                asm.addInstr('sub eax, ' + op1 )
                asm.addInstr('mov ' + res + ', eax')


            else:
                asm.addInstr('mov eax, ' + op1 )
                if op == '-i':
                    asm.addInstr('sub eax, ' + op2 )
                else:
                    asm.addInstr('fsub eax, ' + op2 )
                asm.addInstr('mov ' + res + ', eax')

        elif op in ['*', '*i', '*f']:
            asm.addInstr('mov eax, ' + op1 )
            asm.addInstr('mov ebx, ' + op2 )
            if op == '*i' or op == '*':
                asm.addInstr('imul eax, ebx' )
            else:
                asm.addInstr('fmul eax, ebx' )
            #Storing only the higher 32 bits in EDX
            #Note that the 64 bit result is in EDX:EAX
            asm.addInstr('mov ' + res + ', eax' )

        elif op in ['/', '/i', '/f']:
            asm.addInstr('mov eax, ' + op1 )
            asm.addInstr('mov ebx, ' + op2 )
            if op == '/i' or op == '/':
                asm.addInstr('push edx')
                asm.addInstr('xor edx, edx')
                asm.addInstr('div ebx' )
                asm.addInstr('pop edx')
            else:
                asm.addInstr('fdiv ebx' )
            #Storing only the higher 32 bits in EDX
            #Note that the 64 bit result is in EDX:EAX
            asm.addInstr('mov ' + res + ', eax' )


        elif op in ['=', '=i', '=f', '=p']:

            try:
                asm.funcList[op1]
            except:
                if type(curQuad[1]) == 'pointer':
                    asm.addInstr('mov eax, ' + op1)
                    asm.addInstr('mov eax, [eax]')

                elif type(op1) == 'id':
                    asm.addInstr('mov  eax' + ', [' + op1 + ']')
                elif type(op1) == 'ret_val':
                    pass # ret_val already in eax
                else:
                    asm.addInstr('mov  eax' + ', ' + op1 )

            if type(curQuad[0]) == 'pointer':
                asm.addInstr('mov ebx, ' + res)
                asm.addInstr('mov [ebx], eax')
            if type(res) == 'id':
                asm.addInstr('mov [' + res + '], eax')
            else:
                asm.addInstr('mov ' + res + ', eax')


        elif op in ['+=i']:
                asm.addInstr('mov eax, ' + res )
                asm.addInstr('add eax, ' + op2)
                asm.addInstr('mov ' + res + ', eax')


        elif op in ['<=i', '<=f', '>=i', '>=f', '<i', '<f', '>i', '>f']:
            if type(op1) in ['int', 'float']:
                asm.addInstr('mov eax ' + op1 )
            elif type(id) == 'id':
                asm.addInstr('mov eax, [' + op1 + ']')
            else:
                asm.addInstr('mov eax, ' + op1)
            asm.addInstr('cmp eax, ' + op2)
            jtrue = jumpInstr[op][0]
            jfalse = jumpInstr[op][1]
            asm, j = relOp(asm, quadList, curQuad, j, jtrue, jfalse)
            asm, j = relOp(asm, quadList, curQuad, j, jtrue, jfalse)


        elif op == 'jtrue':
            prev1 = asm.quadList[j - 1][3]
            prev2 = asm.quadList[j - 2][3]

            if prev1 in ['jtrue', 'jfalse'] and prev2 in ['jtrue', 'jfalse']:
                opCmp = asm.quadList[j - 3][3]
                try:
                    jtrue = jumpInstr[opCmp][0]
                except:
                    jtrue = 'jz'
            else:
                    jtrue = 'jz'

            if op2 != '':
                label = newLabel(asm, op2)
                asm.labelQueue.append((int(op2), label ))
                asm.addInstr(jtrue + " " + label)


        elif op == 'jfalse':
            tmp = j - 1
            while asm.quadList[ tmp ][3] == 'scope_begin':
                tmp -= 1

            prev1 = asm.quadList[tmp][3]
            prev2 = asm.quadList[tmp - 1][3]

            if prev1 in ['jtrue', 'jfalse'] and prev2 in ['jtrue', 'jfalse']:
                opCmp = asm.quadList[tmp - 2][3]
                try:
                    jfalse = jumpInstr[opCmp][1]
                except:
                    jfalse = 'jnz'
            else:
                jfalse = 'jnz'

            if op2 != '':
                label = newLabel(asm, op2)
                asm.labelQueue.append((int(op2), label ))
                asm.addInstr(jfalse + " " + label)


        elif op == 'func_begin':
            fname = curQuad[1]
            label = '_' + fname + ':'
            func = asm.funcList[fname]
            func.label = label
            func.scope = scopeTree.searchScope(fname)

            asm.curFunc =  func # add other instructions to function text
            asm.addInstr("\n" + label) # add label to global text
            numTabs += 1
            asm.addInstr('push ebp')
            asm.addInstr('mov ebp, esp' + "; call function")
            asm.createActRecord(func, funcParamList[func.name])


        elif op == 'retq_func_exit':
            if quadList[j - 1][3] not in ['retq_value', 'retq_null']:
                asm.addInstr('mov esp, ebp')
                asm.addInstr('pop ebp')
            func = asm.curFunc
            if func != 'global':
                for i in func.sizeList: #Backpatch
                    bkFunc = i[0]
                    bkInd = i[1]
                    bkFunc.text[bkInd][1] = str(func.offsetSum)
                    bkFunc.text[bkInd] = ''.join(bkFunc.text[bkInd])

            if quadList[j - 1][3] != 'retq_value':
                asm.addInstr('ret')
            asm.curFunc = 'global'
            numTabs = 0

        elif op == 'retq_value':
            if type(op1) == 'id':
                asm.addInstr('mov eax, [' + op1 + ']')
            else:
                if type(op1) == 'ret_val':
                    pass
                else:
                    asm.addInstr('mov eax, ' + op1)
            asm.addInstr('mov esp, ebp')
            asm.addInstr('pop ebp')
            asm.addInstr('ret')

        elif op == 'retq_null':
            asm.addInstr('mov esp, ebp')
            asm.addInstr('pop ebp')
            asm.addInstr('ret')


        elif op == '[]=':
            arrOffset = -func.offsets[curQuad[1]]
            asm.addInstr('mov eax, ' + op2 + "; start offset extraction")
            asm.addInstr('add eax, ' + str(arrOffset))
            asm.addInstr('mov ebx, ebp')
            asm.addInstr('sub ebx, eax')
            asm.addInstr('mov ' + res + ', ebx ;end offset extraction')

        elif op == 'scope_begin':
            func = asm.curFunc
            scopeName = curQuad[1]
            scope = scopeTree.searchScope(scopeName)
            # scopeTree.printTree(scopeTree.root)
            asm.createActRecord(func, [], scope)  


        elif op == 'push':
            if type(op1) == 'id':
                asm.addInstr('push [' + op1 +']')
            elif type(op1) == 'p_ind':
                asm.addInstr('mov eax, ' + op1)
                asm.addInstr('push eax')
            else:
                asm.addInstr('push ' + op1 )


        elif op == 'call':
            calleeName = curQuad[1]
            callee = asm.funcList[calleeName]
            asm.addInstr('call _' + calleeName)

        elif op == 'printf':
            if type(op1) in ['int', 'float', 'char']:
                asm.addInstr('mov eax, ' +  op1 + '; print sequence')
            else:
                if type(op1) == 'id':
                    asm.addInstr('mov eax, [' +  op1 + ']')
                elif type(op1) == 'ret_val':
                    pass #ret_val already in eax
                else:
                    asm.addInstr('mov eax, ' +  op1 )

            asm.addInstr('push eax ;start print sequence')
            asm.addInstr('push printf_' + op2)
            asm.addInstr('call printf')
            asm.addInstr('add esp, 8 ; end print sequence')

        elif op in [ '==i', '==f','==c', '==p','==']:
            if type(op1) == 'id':
                op1 = '[' + op1 + ']'
            if type(op2) == 'id':
                op2 = '[' + op2 + ']'
            asm.addInstr('mov eax, ' + op1)
            asm.addInstr('cmp eax, '+ op2)

        elif op == 'goto':
            label = newLabel(asm, op2)
            asm.labelQueue.append((int(op2), label))
            asm.addInstr('jmp ' + label)

        # elif op == ''

        j += 1
    asm.writeAsm()

def relOp(asm, quadList, i, j, jtrue, jfalse):
    if quadList[j + 1][3] not in ['jtrue', 'jfalse']:
        return
    j += 1
    checkInsertLabel(asm, j)

    i = quadList[j]
    res = i[0]; op1 = i[1]; op2 = i[2]; op = i[3]
    res, op1, op2 = getOperands([res, op1, op2], asm.curFunc)
    
    if op2 != '':
        label = newLabel(asm, op2) 
        asm.labelQueue.append((int(op2), label))
        if op == 'jtrue':
            asm.addInstr(jtrue + ' ' +  label)
        elif op == 'jfalse':
            asm.addInstr(jfalse + ' ' + label)
    return asm, j;

def checkInsertLabel(asm, j):
    for i in asm.labelPositions:
        if j + 1 == i[0]:
            label = i[2]
            asm.addInstr(label + ":")
            
    # if len(asm.labelQueue) > 0 and j + 1 == asm.labelQueue[0][0]: 
        # label = asm.labelQueue.popleft()[1]
        # asm.addInstr(label)

def newLabel(asm, op2):
    global labelCnt
    check = 0
    for i in asm.labelPositions:
        if int(op2) == i[0]:
            label = i[2]
            check = 1
            break
    if check == 0:
        labelCnt += 1
        label = '_L' + str(labelCnt) 
    return label

def getOperands(opList, funcType):
    newOpList = []
    for i in opList:
        if i == '':
            pass
        elif type(i) in ['id', 'pointer']:
            if type(i) == 'pointer':
                i = i[1:]
            if funcType == 'global':
                i = '[' + i + ']'
            else:
                try:
                    offset = funcType.offsets[i]
                    if offset > 0:
                        i = '[ebp + ' + str(offset) +  ']'
                    elif offset < 0:
                        i = '[ebp ' + str(offset) +  ']'
                    else:
                        i = '[ebp]'
                except:
                    waste_time = 1
                    # print(bcolors.WARNING + "Unnecessary temporary variable: " + i + bcolors.ENDC)
        elif type(i) == 'int':
            i = str(int(i))
        elif type(i) == 'float':
            i = str(float(i))
        else:
            waste_time = 1
            # print("getOperands: UNDEFINED TYPE")
        newOpList.append(i)
    return newOpList

def type(i):
    try:
        int(i)
        return 'int'
    except:
        try:
            float(i)
            return 'float'
        except:
            if isinstance(i, str):
                if i[0] == '*':
                    return 'pointer'
                elif i[0] == '[':
                    return 'p_ind'
                elif '(' in i:
                    return 'ret_val'
                else:
                    return 'id'
            else:
                return 'unknown'
