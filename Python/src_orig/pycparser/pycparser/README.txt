GROUP 45: C Compiler in Python
Rushab Munot (14405)
Devendra Meena (14214)

To run the parser on a specific file:
python parser.py <file_loation>

For operator type checking run this program as 
python parser.py <file_location> -AST 1

To execute the assembly code 
./x86.sh

The sample codes are kept in the directory: sample_programs/

The assembly output is generated in the file: bin/output.asm

The three address code is generated in the file: bin/TACFile.csv

References:
Grammar - lex and yacc files : 
[https://github.com/eliben/pycparser](https://github.com/eliben/pycparser)

