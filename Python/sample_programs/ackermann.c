#include<stdio.h>
#include<stdlib.h>
int ackermann(int , int );
int count = 0, indent = 0;
int main()
{
    int x = 3, y = 5;
    int result = ackermann(x,y);
    printf(result);
}
int ackermann(int x, int y)
{
    count = count + 1;
    if(x<0 || y<0){
        return -1;
    }
    if(x==0){
        return y+1;
    }
    if(y==0){
        int temp = ackermann(x-1,1);
        return temp;
    }
    int temp1 = ackermann(x,y-1);
    int temp2  = ackermann(x-1, temp1 );
    return temp2;
}
