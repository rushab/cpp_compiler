
########################################

********Running the Makefile************

########################################

make ARGS=<file-path> all


#######################################

********Features Implemented***********

#######################################

1. Scanner for standard C.
2. Parser for standard C.
3. Multiple parse error handling with a pointer to the error location.
4. Parse Tree generation using dot Tool provided by graphviz.
5. Preprocessor directives parsing included with certain small restrictions.



#########################################

***************Tools Used****************

#########################################

1. flex, yacc(bison) and graphviz( dot )




##########################################

*************Acknowledgement**************

##########################################

1.Source for token specification: https://www.lysator.liu.se/c/ANSI-C-grammar-l.html

2.Source for grammar: https://www.lysator.liu.se/c/ANSI-C-grammar-y.html

3.Source for grammar of preprocessor directives: http://homepages.e3.net.nz/~djm/cppgrammar.html#preprocessing-file
