//void openFile();

char* genMul(char* op, char* rand1, char* rand2);
char* genAdd(char* op, char* rand1, char* rand2);
char* genConst(uint i, char* label);
char* genType(char*s , char* label);

char* genN(char* s, char* label);
char* gen2N(char*s1, char* s2, char* label);
char* gen3N(char*s1, char* s2, char* s3, char* label);
char* gen4N(char*s1, char* s2, char* s3, char* s4, char* label);

char* genTT(char* rand1, char* rand2, char* label);
char* gen3T( char* rand1, char* rand2, char* rand3, char* label );

char* genNTN(char* s1, char* rand1, char* s2, char* label);
char* genNTNT(char* s1, char* rand1, char* s2, char* rand2, char* label);

char* genNTT(char* s, char* rand1, char* rand2, char* label);
char* genNT(char* s, char* rand1, char* label); 


char* genTNT(char* rand1, char* s, char* rand2, char* label );
char* genTypeTypeT( char* type1, char* type2, char* ter, char* label );
char* genTypeNT(char* type, char* s, char* rand1, char* label);
char* genTypeT(char* type, char* rand1, char* label);
char* genT(char* rand1, char* label);



char* genTNTN(char* rand1, char* s1, char* rand2, char* s2, char* label);
char* genTNTT(char* rand1, char* s, char* rand2, char* rand3 ,char* label );
char* genTTNT(char* rand1, char* rand2, char* s, char* rand3, char* label);

char* genTN(char* rand1, char* s, char* label);


char* genNNT( char* s1, char* s2, char*rand1, char* label );

char* genTTTNT(char* rand1, char*rand2, char* rand3, char* s ,char*rand5, char* label);
char* genNTTNT(char* s1, char* rand1, char* rand2, char* s2 ,char*rand3, char* label);

char* genNTNTN(char* s1, char* rand1, char* s2, char* rand2, char* s3, char* label);
char* genTTNTN(char* rand1, char* rand2, char* s1, char* rand3, char* s2, char* label);

char* genTTNTNTN(char* rand1, char* rand2, char* s1, char* rand3, char* s2, char* rand4, char* s3, char* label);
char* genTNNN(char* rand1, char* s1, char* s2, char* s3, char* label);


char* genTNNT(char* rand1, char* s1, char* s2, char* rand2, char* label);

char* genTTN(char* rand1, char* rand2, char* s, char* label);
char* genTNN(char* rand1, char* s1, char* s2, char* label);
char* genTTTN(char* rand1, char*rand2, char* rand3, char* s1, char* label);
char* genTTNN(char* rand1, char*rand2, char* s1 ,char* s2, char* label);
char* genTTTNN(char* rand1, char*rand2, char* rand3, char* s1 ,char* s2, char* label);
char* genTTNNN(char* rand1, char* rand2, char *s1, char* s2, char*s3, char* label);

char* genTNTTNTT(char* rand1, char* s1, char* rand2, char* rand3, char* s2, char* rand4, char* rand5, char* label);

char* genTTNNNTN(char* rand1, char* rand2, char *s1, char* s2, char*s3, char* rand3, char* s4, char* label);

char* genTTNNTN(char* rand1, char* rand2, char *s1, char* s2, char* rand3, char* s3, char* label);


