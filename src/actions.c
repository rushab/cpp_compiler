#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "actions.h"

/*FILE* f;*/


/*void openFile(){*/
    /*f= fopen("graph.dot","a");*/
/*}*/


char* genMul(char* op, char* rand1, char* rand2){
    static int opNum = 0;
    opNum++;
    uint mylen = strlen(op) + strlen("mul") + 20;
    char* me = malloc(sizeof(char)*mylen);
    sprintf(me, "mul%d", opNum);
    FILE* f = fopen("graph.dot", "a");
    if (strcmp(rand2,"-1") == 0)
        { fprintf(f, "%s->%s;\n%s[label=\"mul_expr\"]\n", me, rand1, me);
        }
    else{ 
        fprintf(f, "%s->{%s mulOp%d %s}\n%s[label = \"mulExpr\"]\nmulOp%d[label=\"%s\"]\n", me, rand1, opNum, rand2, me, opNum, op);
    }
    fclose(f);
    return me;
}

char* genAdd(char* op, char* rand1, char* rand2){
    static int addNum = 0;
    addNum++;
    uint mylen = strlen(op) + strlen("add") + 20;
    char* me = malloc(sizeof(char)*mylen);
    sprintf(me, "add%d", addNum);
    FILE* f = fopen("graph.dot", "a");
    if (strcmp(rand2,"-1") == 0)
        fprintf(f, "%s->%s;\n%s[label=\"add_expr\"]\n", me, rand1, me);
    else 
        fprintf(f, "%s->{%s addOp%d %s}\n%s[label = \"addExpr\"]\naddOp%d[label=\"%s\"]\n", me, rand1, addNum, rand2, me, addNum, op);
    fclose(f);
    return me;
}


char* genConst(uint i, char* label){
    static int constNum =0;
    constNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "const%d",constNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->constVal%d\n%s[label=\"%s\"]\nconstVal%d[label=\"%u\"]\n",me,constNum,me,label,constNum,i);
    fclose(f);
    return me;
}



char* genN(char* s, char* label){
    static int nNum = 0;
    nNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "N%d", nNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->%s\n%s[label=\"%s\"]\n", me, s, me, label);
    fclose(f);
    return me;
}


char* gen2N(char*s1, char* s2, char* label){
    static int nnNum = 0;
    nnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me,"NN%d",nnNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{ %s %s}\n%s[label = \"%s\"]\n",me,s1,s2,me,label);
    fclose(f);
    return me;
}

char* gen3N(char*s1, char* s2, char* s3, char* label){
    static int nnnNum = 0;
    nnnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me,"NNN%d",nnnNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{ %s %s %s}\n%s[label = \"%s\"]\n",me,s1,s2,s3,me,label);
    fclose(f);
    return me;
}


char* gen4N(char*s1, char* s2, char* s3, char* s4, char* label){
    static int n4Num = 0;
    n4Num++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me,"NNNN%d",n4Num);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{ %s %s %s %s}\n%s[label = \"%s\"]\n",me,s1,s2,s3,s4,me,label);
    fclose(f);
    return me;
}

char* genNTN(char* s1, char* rand1, char* s2, char* label){
    static int ntnNum = 0;
    ntnNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me, "NTN%d", ntnNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{%s NTNter%d %s}\n", me, s1, ntnNum, s2);
    fprintf(f,"%s[label=\"%s\"]\n", me, label) ;
    fprintf(f,"NTNter%d[label=\"%s\"]\n", ntnNum, rand1);
    fclose(f);
    return me;
}

char* genNTNT(char* s1, char* rand1, char* s2, char* rand2, char* label){
    static int ntntNum = 0;
    ntntNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me, "NTNT%d", ntntNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{%s NTNT1Ter%d %s NTNT2Ter%d}\n", me, s1, ntntNum, s2, ntntNum);
    fprintf(f,"%s[label=\"%s\"]\n", me, label) ;
    fprintf(f,"NTNT1Ter%d[label=\"%s\"]\n", ntntNum, rand1);
    fprintf(f,"NTNT2Ter%d[label=\"%s\"]\n", ntntNum, rand2);
    fclose(f);
    return me;
}



char* genTNT(char* rand1, char* s, char* rand2, char* label){
    static int tntNum = 0;
    tntNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TNT%d", tntNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{ TNT1Ter%d %s TNT2Ter%d }\n", me, tntNum, s, tntNum) ;
    fprintf(f,"TNT1Ter%d[label=\"%s\"]\n", tntNum, rand1) ;
    fprintf(f,"TNT2Ter%d[label=\"%s\"]\n", tntNum, rand2) ;
    fprintf(f,"%s[label=\"%s\"]\n", me, label) ;
    fclose(f);
    return me;
}

char* genNTT(char *s, char* rand1, char* rand2, char* label){
    static int nttNum = 0;
    nttNum++;;
    char* me = malloc(sizeof(char)*20);
    sprintf(me,"NTT%d",nttNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f, "%s->{ %s NTT1Ter%d NTT2Ter%d }\n", me, s, nttNum, nttNum );
    fprintf(f, "%s[label=\"%s\"]", me, label);
    fprintf(f, "NTT1Ter%d[label=\"%s\"]\n", nttNum, rand1);
    fprintf(f, "NTT2Ter%d[label=\"%s\"]\n", nttNum, rand2);
    fclose(f);
    return me;
}


char* genNT(char* s, char* rand1, char* label){
    static int ntNum = 0;
    ntNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"NT%d",ntNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{%s NTVal%d}\n",me,s,ntNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"NTVal%d[label=\"%s\"]\n",ntNum,rand1);
    fclose(f);
    return me;
}

char* gen3T( char* rand1, char* rand2, char* rand3, char* label ){
    static int tttNum = 0;
    tttNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"TTT%d",tttNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{TTT1ter%d TTT2ter%d TTT3ter%d}\n", me, tttNum, tttNum, tttNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TTT1ter%d[label=\"%s\"]\n", tttNum, rand1);
    fprintf(f,"TTT2ter%d[label=\"%s\"]\n", tttNum, rand2);
    fprintf(f,"TTT3ter%d[label=\"%s\"]\n", tttNum, rand3);
    fclose(f);
    return me;
}

char* genTT(char* rand1, char* rand2, char* label){
    static int ttNum = 0;
    ttNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"TT%d",ttNum);
    FILE* f = fopen("graph.dot", "a");
    fprintf(f,"%s->{TT1ter%d TT2ter%d}\n",me, ttNum, ttNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TT1ter%d[label=\"%s\"]\n", ttNum, rand1);
    fprintf(f,"TT2ter%d[label=\"%s\"]\n", ttNum, rand2);
    fclose(f);
    return me; 
}

char* genT(char* rand1, char* label){
    static int tNum = 0;
    tNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"T%d",tNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f,"%s->Tter%d\n", me, tNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"Tter%d[label=\"%s\"]\n", tNum, rand1);
    fclose(f);
    return me;
}


char* genTNTN(char* rand1, char* s1, char* rand2, char* s2, char* label){
    static int tntnNum = 0;
    tntnNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"TNTN%d",tntnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f,"%s->{ TNTN1Ter%d %s TNTN2Ter%d %s }\n", me, tntnNum, s1, tntnNum, s2);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TNTN1Ter%d[label=\"%s\"]\n", tntnNum, rand1);
    fprintf(f,"TNTN2Ter%d[label=\"%s\"]\n", tntnNum, rand2);
    fclose(f);
    return me;
}


char* genTNTT(char* rand1, char* s, char* rand2, char* rand3 ,char* label ){
    static int tnttNum = 0;
    tnttNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"TNTT%d",tnttNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f,"%s->{ TNTT1Ter%d %s TNTT2Ter%d TNTT3Ter%d }\n", me, tnttNum, s, tnttNum, tnttNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TNTT1Ter%d[label=\"%s\"]\n", tnttNum, rand1);
    fprintf(f,"TNTT2Ter%d[label=\"%s\"]\n", tnttNum, rand2);
    fprintf(f,"TNTT3Ter%d[label=\"%s\"]\n", tnttNum, rand3);
    fclose(f);
    return me;
}

char* genTTNT(char* rand1, char* rand2, char* s, char* rand3, char* label){
    static int ttntNum = 0;
    ttntNum++;
    char*me = malloc(sizeof(char)*20);
    sprintf(me,"TTNT%d",ttntNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f,"%s->{ TTNT1Ter%d %s TTNT2Ter%d TTNT3Ter%d }\n", me, ttntNum, s, ttntNum, ttntNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TTNT1Ter%d[label=\"%s\"]\n", ttntNum, rand1);
    fprintf(f,"TTNT2Ter%d[label=\"%s\"]\n", ttntNum, rand2);
    fprintf(f,"TTNT3Ter%d[label=\"%s\"]\n", ttntNum, rand3);
    fclose(f);
    return me;
}


char* genTN(char* rand1, char* s, char* label){
    static int tnNum = 0;
    tnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TN%d", tnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TN1Ter%d %s}\n", me, tnNum, s);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TN1Ter%d[label=\"%s\"]\n", tnNum, rand1);
    fclose(f);
    return me;
}


char* genNNT( char* s1, char* s2, char*rand1, char* label ){
    static int nntNum = 0;
    nntNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "NNT%d",nntNum);
    FILE * f = fopen("graph.dot","a");
    fprintf(f,"%s->{%s %s NNT1Ter%d}\n",me, s1, s2, nntNum);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "NNT1Ter%d[label=\"%s\"]\n", nntNum, rand1);
    fclose(f);
    return me;
}

char* genNTNTN(char* s1, char* rand1, char* s2, char* rand2, char* s3, char* label){
    static int ntntnNum = 0;
    ntntnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "NTNTN%d", ntntnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{%s NTNTN1Ter%d %s NTNTN2Ter%d %s}\n", me, s1, ntntnNum, s2, ntntnNum, s3);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "NTNTN1Ter%d[label=\"%s\"]\n]", ntntnNum, rand1);
    fprintf(f, "NTNTN2Ter%d[label=\"%s\"]\n]", ntntnNum, rand2);
    fclose(f);
    return me;
}

char* genTTTNT(char* rand1, char*rand2, char* rand3, char* s ,char*rand4, char* label){
    static int tttntNum = 0;
    tttntNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TTTNT%d", tttntNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TTTNT1Ter%d TTTNT2Ter%d TTTNT3Ter%d %s TTTNT4Ter%d}\n", me, tttntNum, tttntNum, tttntNum, s, tttntNum);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TTTNT1Ter%d[label=\"%s\"]\n]", tttntNum, rand1);
    fprintf(f, "TTTNT2Ter%d[label=\"%s\"]\n]", tttntNum, rand2);
    fprintf(f, "TTTNT3Ter%d[label=\"%s\"]\n]", tttntNum, rand3);
    fprintf(f, "TTTNT4Ter%d[label=\"%s\"]\n]", tttntNum, rand4);
    fclose(f);
    return me;
}
char* genTTTNN(char* rand1, char*rand2, char* rand3, char* s1 ,char* s2, char* label){
    static int tttnnNum = 0;
    tttnnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TTTNN%d", tttnnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TTTNN1Ter%d TTTNN2Ter%d TTTNN3Ter%d %s %s}\n", me, tttnnNum, tttnnNum, tttnnNum, s1, s2);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TTTNN1Ter%d[label=\"%s\"]\n]", tttnnNum, rand1);
    fprintf(f, "TTTNN2Ter%d[label=\"%s\"]\n]", tttnnNum, rand2);
    fprintf(f, "TTTNN3Ter%d[label=\"%s\"]\n]", tttnnNum, rand3);
    fclose(f);
    return me;
}

char* genTTTN(char* rand1, char*rand2, char* rand3, char* s1, char* label){
    static int tttnNum = 0;
    tttnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TTTN%d", tttnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TTTN1Ter%d TTTN2Ter%d TTTN3Ter%d %s }\n", me, tttnNum, tttnNum, tttnNum,  s1);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TTTN1Ter%d[label=\"%s\"]\n]", tttnNum, rand1);
    fprintf(f, "TTTN2Ter%d[label=\"%s\"]\n]", tttnNum, rand2);
    fprintf(f, "TTTN3Ter%d[label=\"%s\"]\n]", tttnNum, rand3);
    fclose(f);
    return me;
}

char* genNTTNT(char* s1, char* rand1, char* rand2, char* s2 ,char*rand3, char* label){
    static int nttntNum = 0;
    nttntNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "NTTNT%d", nttntNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{%s NTTNT1Ter%d NTTNT2Ter%d %s NTTNT3Ter%d}\n", me, s1, nttntNum, nttntNum, s2, nttntNum);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "NTTNT1Ter%d[label=\"%s\"]\n", nttntNum, rand1);
    fprintf(f, "NTTNT2Ter%d[label=\"%s\"]\n", nttntNum, rand2);
    fprintf(f, "NTTNT3Ter%d[label=\"%s\"]\n", nttntNum, rand3);
    fclose(f);
    return me;
}

char* genTTNTN(char* rand1, char* rand2, char* s1, char* rand3, char* s2, char* label){
    static int ttntnNum = 0;
    ttntnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TTNTN%d", ttntnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TTNTN1Ter%d TTNTN2Ter%d %s TTNTN3Ter%d %s}\n", me,ttntnNum, ttntnNum, s1, ttntnNum, s2);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TTNTN1Ter%d[label=\"%s\"]\n", ttntnNum, rand1);
    fprintf(f, "TTNTN2Ter%d[label=\"%s\"]\n", ttntnNum, rand2);
    fprintf(f, "TTNTN3Ter%d[label=\"%s\"]\n", ttntnNum, rand3);
    fclose(f);
    return me;
}

char* genTTNTNTN(char* rand1, char* rand2, char* s1, char* rand3, char* s2, char* rand4, char* s3, char* label){
    static int ttntntnNum = 0;
    ttntntnNum++;
    char*me = malloc(sizeof(char)* 20);
    sprintf(me, "TTNTNTN%d", ttntntnNum);
    FILE* f = fopen("graph.dot","a");
    fprintf(f, "%s->{TTNTNTN1Ter%d TTNTNTN2Ter%d %s TTNTNTN3Ter%d %s TTNTNTN4Ter%d %s}\n", me, ttntntnNum, ttntntnNum, s1, ttntntnNum, s2, ttntntnNum, s3);
    fprintf(f, "%s[label=\"%s\"]\n", me, label);
    fprintf(f, "TTNTNTN1Ter%d[label=\"%s\"]\n", ttntntnNum, rand1);
    fprintf(f, "TTNTNTN2Ter%d[label=\"%s\"]\n", ttntntnNum, rand2);
    fprintf(f, "TTNTNTN3Ter%d[label=\"%s\"]\n", ttntntnNum, rand3);
    fprintf(f, "TTNTNTN4Ter%d[label=\"%s\"]\n", ttntntnNum, rand4);
    fclose(f);
    return me;
}


char* genTNNT(char* rand1, char* s1, char* s2, char* rand2, char* label){
    static int tnntNum =0 ;
    tnntNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TNNT%d", tnntNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{ TNNT1Ter%d %s %s TNNT2Ter%d}\n", me, tnntNum, s1, s2, tnntNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TNNT1Ter%d[label=\"%s\"]\n", tnntNum, rand1);
    fprintf(f,"TNNT2Ter%d[label=\"%s\"]\n", tnntNum, rand2);
    fclose(f);
    return me;
}

char* genTTN(char* rand1, char* rand2, char* s1, char* label){
    static int ttnNum =0 ;
    ttnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TTN%d", ttnNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{ TTN1Ter%d TTN2Ter%d %s}\n", me, ttnNum, ttnNum, s1);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TTN1Ter%d[label=\"%s\"]\n",ttnNum, rand1);
    fprintf(f,"TTN2Ter%d[label=\"%s\"]\n",ttnNum, rand2);
    fclose(f);
    return me;    
}

char* genTNN(char* rand1, char* s1, char* s2, char* label){
    static int tnnNum =0 ;
    tnnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TNN%d", tnnNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{ TNN1Ter%d %s %s}\n", me, tnnNum, s1, s2);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TNN1Ter%d[label=\"%s\"]\n",tnnNum, rand1);
    fclose(f);
    return me;    
    
}

char* genTNNN(char* rand1, char* s1, char* s2, char* s3, char* label){
    static int tnnnNum =0 ;
    tnnnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TNN%d", tnnnNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{ TNNN1Ter%d %s %s %s}\n", me, tnnnNum, s1, s2, s3);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TNNN1Ter%d[label=\"%s\"]\n",tnnnNum, rand1);
    fclose(f);
    return me;        
}

char* genTNTTNTT(char* rand1, char* s1, char* rand2, char* rand3, char* s2, char* rand4, char* rand5, char* label){
    static int whileNum = 0;
    whileNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "WHILE%d", whileNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{WHILE1Ter%d %s WHILE2Ter%d WHILE3Ter%d %s WHILE4Ter%d WHILE5Ter%d}\n", me, whileNum, s1, whileNum, whileNum, s2, whileNum, whileNum);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"WHILE1Ter%d[label=\"%s\"]\n", whileNum, rand1);
    fprintf(f,"WHILE2Ter%d[label=\"%s\"]\n", whileNum, rand2);
    fprintf(f,"WHILE3Ter%d[label=\"%s\"]\n", whileNum, rand3);
    fprintf(f,"WHILE4Ter%d[label=\"%s\"]\n", whileNum, rand4);
    fprintf(f,"WHILE5Ter%d[label=\"%s\"]\n", whileNum, rand5);
    fclose(f);
    return me;
}

char* genTTNN(char* rand1, char* rand2, char *s1, char* s2, char* label){
    static int ttnnNum = 0;
    ttnnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TTNN%d", ttnnNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{TTNN1Ter%d TTNN2Ter%d %s %s }\n", me, ttnnNum, ttnnNum, s1, s2 );
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TTNN1Ter%d[label=\"%s\"]\n", ttnnNum, rand1);
    fprintf(f,"TTNN2Ter%d[label=\"%s\"]\n", ttnnNum, rand2);
    fclose(f);
    return me;
}

char* genTTNNN(char* rand1, char* rand2, char *s1, char* s2, char*s3, char* label){
    static int ttnnnNum = 0;
    ttnnnNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "TTNNN%d", ttnnnNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{TTNNN1Ter%d TTNNN2Ter%d %s %s %s}\n", me, ttnnnNum, ttnnnNum, s1, s2, s3 );
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"TTNNN1Ter%d[label=\"%s\"]\n", ttnnnNum, rand1);
    fprintf(f,"TTNNN2Ter%d[label=\"%s\"]\n", ttnnnNum, rand2);
    fclose(f);
    return me;
}

char* genTTNNNTN(char* rand1, char* rand2, char *s1, char* s2, char*s3, char* rand3, char* s4, char* label){
    static int forNum = 0;
    forNum++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "FOR%d", forNum);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{FOR1Ter%d FOR2Ter%d %s %s %s FOR3Ter%d %s}\n", me, forNum, forNum, s1, s2, s3, forNum, s4);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"FOR1Ter%d[label=\"%s\"]\n", forNum, rand1);
    fprintf(f,"FOR2Ter%d[label=\"%s\"]\n", forNum, rand2);
    fprintf(f,"FOR3Ter%d[label=\"%s\"]\n", forNum, rand3);
    fclose(f);
    return me;
}

char* genTTNNTN(char* rand1, char* rand2, char *s1, char* s2, char* rand3, char* s3, char* label){
    static int forNum2 = 0;
    forNum2++;
    char* me = malloc(sizeof(char)*20);
    sprintf(me, "FORII%d", forNum2);
    FILE * f = fopen ("graph.dot", "a");
    fprintf(f, "%s->{FORII1Ter%d FORII2Ter%d %s %s FORII3Ter%d %s}\n", me, forNum2, forNum2, s1, s2, forNum2, s3);
    fprintf(f,"%s[label=\"%s\"]\n",me,label);
    fprintf(f,"FORII1Ter%d[label=\"%s\"]\n", forNum2, rand1);
    fprintf(f,"FORII2Ter%d[label=\"%s\"]\n", forNum2, rand2);
    fprintf(f,"FORII3Ter%d[label=\"%s\"]\n", forNum2, rand3);
    fclose(f);
    return me;
}
