%{
#include<stdio.h>
#include<stdlib.h>
#include<stdarg.h>
#include<string.h>
#include "actions.h"

int yylex(void);
void yyerror( char* s );
void pre_checkSetZero();
void eatNewLine();
%}

%nonassoc LEAST

%token <code> IDENTIFIER CONSTANT STRING_LITERAL SIZEOF
%token <code>  PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP

%token <code> AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token <code> SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token <code> XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token <code> TYPEDEF EXTERN STATIC AUTO REGISTER
%token <code> BOOL CHAR  SHORT UINT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE 
%token <code> VOID
%token <code> STRUCT UNION ENUM ELLIPSIS

%token <code> CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token <code> PRE_IF PRE_IFDEF PRE_IFNDEF PRE_ELIF PRE_ELSE PRE_ENDIF PRE_INCLUDE
%token  <code> PRE_DEFINE PRE_UNDEF PRE_LINE PRE_ERROR PRE_PRAGMA 
%token <code> NEWLINE

%type <code> primary_expression postfix_expression argument_expression_list
%type <code> unary_expression unary_operator cast_expression multiplicative_expression
%type <code> additive_expression shift_expression relational_expression equality_expression 
%type <code> and_expression exclusive_or_expression inclusive_or_expression
%type <code> logical_and_expression logical_or_expression conditional_expression
%type <code> assignment_expression assignment_operator expression
%type <code> constant_expression declaration declaration_specifiers
%type <code> init_declarator_list init_declarator storage_class_specifier
%type <code> type_specifier struct_or_union_specifier struct_or_union
%type <code> struct_declaration_list struct_declaration specifier_qualifier_list
%type <code> struct_declarator_list struct_declarator


%type <code> enum_specifier enumerator_list enumerator type_qualifier
%type <code> declarator direct_declarator pointer type_qualifier_list
%type <code> parameter_type_list parameter_list parameter_declaration identifier_list type_name abstract_declarator 
%type <code> direct_abstract_declarator initializer
%type <code> initializer_list statement labeled_statement compound_statement declaration_list statement_list 
%type <code> expression_statement  selection_statement iteration_statement jump_statement translation_unit external_declaration function_definition 

%type <code> preprocessing_file group group_part if_section if_group elif_groups elif_group else_group
%type <code> endif_line control_line  lparen replacement_list pp_tokens new_line
%type <code> preprocessing_token header_name header_middle_name

%type <code> pp_number constant_expr

%start translation_unit

%nonassoc LOWER_THAN_NEWLINE
%nonassoc NEWLINE
%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%nonassoc LOWER_THAN_PREPROCESSOR
%nonassoc PRE_IF PRE_IFDEF PRE_IFNDEF PRE_ELIF PRE_ELSE PRE_ENDIF PRE_INCLUDE PRE_DEFINE PRE_UNDEF PRE_ERROR PRE_PRAGMA  PRE_LINE 

%nonassoc '.' '>' '<' ','
%nonassoc IDENTIFIER CONSTANT STRING_LITERAL 


%nonassoc LESS_THAN_1
%nonassoc LESS_THAN_2
%union{
    int iValue;
    char* code;
}

%%


/*dev code start */
preprocessing_file
       : group %prec LOWER_THAN_PREPROCESSOR { $$ = genN( $1, "preprocessing_file");}
       ;

group
       : group_part %prec LOWER_THAN_PREPROCESSOR   { $$ = genN($1, "group");}
       | group group_part        { $$ = gen2N($1, $2,"group");}
       ;

group_part
       : pp_tokens  new_line            { $$ = gen2N( $1, $2, "group_part" ); }  
       | pp_tokens ';' new_line  %prec LESS_THAN_1    { $$ = genNTN( $1, ";", $3, "group_part" ); } 
       | if_section             { $$ = genN($1 , "group_part");}
       | control_line           { $$ = genN($1 , "group_part");}
       ;

pp_tokens
       : preprocessing_token            { $$ = genN($1,"pp_tokens"); }
       | pp_tokens preprocessing_token  { $$ = gen2N ( $1, $2,"pp_tokens");}
       ;

preprocessing_token 
       : header_name    { $$ = genN( $1, "preprocessing_token" ); }
       | IDENTIFIER     { $$ = genT( $1, "preprocessing_token" ); }
       | STRING_LITERAL { $$ = genT( $1, "preprocessing_token" ); }      
       | '.'            { $$ = genT(".", "preprocessing_token" ); }
       | pp_number  %prec LOWER_THAN_PREPROCESSOR     { $$ = genN( $1, "preprocessing_token" ); }
       ;

header_name
       : '<' header_middle_name '>' { $$ = genTNT( "<", $2, ">","header_name"); }

header_middle_name
       : IDENTIFIER                 { $$ = genT ( $1, "header_middle_name" ); }
       | IDENTIFIER '.' IDENTIFIER  { $$ = gen3T( $1, ".", $3, "header_middle_name"); }

constant_expr
       : conditional_expression     { $$ = genN( $1, "constant_expression" ); }

pp_number
       : CONSTANT           { $$ = genT ( $1 , "pp_number" ); }
       |'.' CONSTANT        { $$ = genTT( ".", $2, "pp_number" ); }
       | pp_number CONSTANT { $$ = genNT( $1 , $2, "pp_number" ); }
       
if_section:
       if_group elif_groups else_group endif_line { $$ = gen4N( $1, $2, $3, $4, "if_section");}
       | if_group endif_line                      { $$ = gen2N( $1, $2, "if_section" ); }  
       ;

if_group
       :  PRE_IF constant_expr new_line group  { $$ = genTNNN($1, $2, $3, $4, "if_group");}
       |  PRE_IFDEF IDENTIFIER new_line group  { $$ = genTTNN($1, $2, $3, $4, "if_group");}
       |  PRE_IFNDEF IDENTIFIER new_line group { $$ = genTTTN($1, $2, $3, $4, "if_group");}
       ;

elif_groups
       : elif_group             { $$ = genN($1,"elif_groups");}
       | elif_groups elif_group { $$ = gen2N($1, $2,"elif_groups");}
       ;

elif_group
       :  PRE_ELIF constant_expr new_line group { $$ = genTNNN($1, $2, $3, $4,"elif_group");}
       ;

else_group
       : PRE_ELSE  new_line group {  $$ = genTNN($1, $2, $3,"else_group");}
       ;

endif_line
       : PRE_ENDIF  new_line  { $$ = genTN($1, $2,"endif_line"); pre_checkSetZero();}
       ;

control_line
       :  PRE_INCLUDE pp_tokens new_line                    { pre_checkSetZero(); $$ = genTNN($1, $2, $3, "control_line"); }
       |  PRE_DEFINE  IDENTIFIER replacement_list new_line  { pre_checkSetZero(); $$ = genTTNN($1, $2, $3, $4,"control_line"); }
       |  PRE_DEFINE  IDENTIFIER lparen identifier_list '('  replacement_list new_line {}
       /*|  PRE_DEFINE  IDENTIFIER lparen ELLIPSIS ')' replacement_list new_line  {}*/

       /*|  PRE_DEFINE  IDENTIFIER lparen identifier_list ',' ELLIPSIS ')' replacement_list new_line {}*/

       |  PRE_UNDEF   IDENTIFIER new_line { $$ = genTTN($1, $2, $3,"control_line");}
       
       |  PRE_LINE    pp_tokens new_line { $$ = genTNN($1, $2, $3,"control_line");}
       |  PRE_ERROR   pp_tokens new_line { $$ = genTNN($1, $2, $3,"control_line");}
       |  PRE_PRAGMA  pp_tokens new_line { $$ = genTNN($1, $2, $3,"control_line");}
       |  new_line                       { $$ = genN($1,"control_line");}
       ;


lparen:
      '('  %prec LOWER_THAN_ELSE        { $$ = gen3T(";;","a","(","lparen"); /* OR (from another grammar) The leftmost parenthesis not preceding a whitespace*/}
       ;

replacement_list
       : pp_tokens %prec LOWER_THAN_NEWLINE  { $$ = genN($1,"replacement_list");}
       | external_declaration                { $$ = genN($1,"replacement_list");}  /*Code by Rushab*/
       ;

new_line
       :NEWLINE         { $$ = genT("newline","new_line");}
       ;

/* dev's code end */

primary_expression
    : IDENTIFIER        { $$ = genT($1, "primary_expression"); }
    | CONSTANT          { $$ = genT($1, "primary_expression");}
    | STRING_LITERAL    { $$ = genT($1, "primary_expression");}
    | '(' expression ')'{ $$ = genTNT( "(", $2 ,")" , "primary_expression"); }
    ;

postfix_expression
    : primary_expression                                  { $$ = genN   ( $1, "postfix_expression");}
    | postfix_expression '[' expression ']'               { $$ = genNTNT( $1, "[", $3 , "]", "postfix_expression");}
    | postfix_expression '(' ')'                          { $$ = genNTT ( $1, "(", ")", "postfix_expression"); }
    | postfix_expression '(' argument_expression_list ')' { $$ = genNTNT( $1, "(", $3 , ")", "postfix_expression");}
    | postfix_expression '.' IDENTIFIER                   { $$ = genNTT ( $1, ".", $3 , "postfix_expression"); }
    | postfix_expression PTR_OP IDENTIFIER                { $$ = gen3N  ( $1, $2 , $3 , "postfix_expression"); }
    | postfix_expression INC_OP                           { $$ = genNT  ( $1, $2 , "postfix_expression"); }
    | postfix_expression DEC_OP                           { $$ = genNT  ( $1, $2 , "postfix_expression"); }
    ;

argument_expression_list
    : assignment_expression                              { $$ = genN($1, "argument_expression_list"); }
    | argument_expression_list ',' assignment_expression { $$ = genNTN( $1, ",", $3, "argument_expression_list" ); } 
    ;

unary_expression
    : postfix_expression        { $$ = genN($1, "unary_expression"); }
    | INC_OP unary_expression   { $$ = genTN($1, $2, "unary_expression"); }
    | DEC_OP unary_expression   { $$ = genTN($1, $2, "unary_expression"); }
    | unary_operator cast_expression   { $$ = gen2N($1, $2, "unary_expression"); }
    | SIZEOF unary_expression   { $$ = genTN($1, $2, "unary_expression"); }  
    | SIZEOF '(' type_name ')'  { $$ = genTTNT($1, "(", $3, ")", "unary_expression"); }
    ;

unary_operator
    : '&'   { $$ = genT("&", "unary_operator"); }
    | '*'   { $$ = genT("*", "unary_operator"); }
    | '+'   { $$ = genT("+", "unary_operator"); }
    | '-'   { $$ = genT("-", "unary_operator"); }
    | '~'   { $$ = genT("~", "unary_operator"); }
    | '!'   { $$ = genT("!", "unary_operator"); }
    ;

cast_expression
    : unary_expression                   { $$ = genN($1,"cast_expression"); }
    | '(' type_name ')' cast_expression  {$$ = genTNTN("(", $2, ")", $4, "cast_expression");}
    ;

multiplicative_expression
    : cast_expression                               { $$ = genMul("NULL", $1, "-1"); }
    | multiplicative_expression '*' cast_expression { $$ = genMul("*", $1, $3);}
    | multiplicative_expression '/' cast_expression { $$ = genMul("/", $1, $3); }
    | multiplicative_expression '%' cast_expression { $$ = genMul("%", $1, $3); }
    ;

additive_expression
    : multiplicative_expression                         { $$ = genAdd( "NULL", $1 , "-1"); }
    | additive_expression '+' multiplicative_expression { $$ = genAdd("+", $1, $3 ); }
    | additive_expression '-' multiplicative_expression { $$ = genAdd("-", $1, $3 ); }
    ;

shift_expression
    : additive_expression                           { $$ = genN($1, "shift_expression"); }
    | shift_expression LEFT_OP additive_expression  { $$ = genNTN( $1, $2, $3, "shift_expression" ); } 
    | shift_expression RIGHT_OP additive_expression { $$ = genNTN( $1, $2, $3, "shift_expression" ); }
    ;

relational_expression
    : shift_expression                              { $$ = genN($1, "relational_expression"); }
    | relational_expression '<' shift_expression    { $$ = genNTN( $1, "<", $3, "relational_expression"); }
    | relational_expression '>' shift_expression    { $$ = genNTN( $1, "<", $3, "relational_expression"); }
    | relational_expression LE_OP shift_expression  { $$ = genNTN( $1, $2, $3, "relational_expression" ); }
    | relational_expression GE_OP shift_expression  { $$ = genNTN( $1, $2, $3, "relational_expression" ); }
    ;

equality_expression
    : relational_expression                             { $$ = genN($1, "equality_expression"); }
    | equality_expression EQ_OP relational_expression   { $$ = genNTN( $1, $2, $3, "equality_expression" ); }
    | equality_expression NE_OP relational_expression   { $$ = genNTN( $1, $2, $3, "equality_expression" ); }
    ;

and_expression
    : equality_expression                       { $$ = genN( $1, "and_expression"); }
    | and_expression '&' equality_expression    { $$ = genNTN( $1, "&", $3, "and_expression" ); }
    ;

exclusive_or_expression
    : and_expression                                { $$ = genN($1, "exclusive_or_expression"); }
    | exclusive_or_expression '^' and_expression    { $$ = genNTN($1, "^", $3, "exclusive_or_expression"); }
    ;

inclusive_or_expression
    : exclusive_or_expression                               { $$ = genN($1, "inclusive_or_expression"); }
    | inclusive_or_expression '|' exclusive_or_expression   { $$ = genNTN($1, "|", $3, "inclusive_or_expression"); }
    ;

logical_and_expression
    : inclusive_or_expression                               { $$ = genN( $1, "logical_and_expression" );  }
    | logical_and_expression AND_OP inclusive_or_expression { $$ = genNTN( $1, $2, $3, "logical_and_expression" ); }
    ;

logical_or_expression
    : logical_and_expression                                { $$ = genN( $1, "logical_or_expression" ); }
    | logical_or_expression OR_OP logical_and_expression    { $$ = genNTN( $1, $2, $3, "logical_or_expression" ); }
    ;

conditional_expression
    : logical_or_expression                                             { $$ = genN( $1, "conditional_expression" ); }
    | logical_or_expression '?' expression ':' conditional_expression   { $$ = genNTNTN( $1, "?", $3, ":", $5, "conditional_expression"); }
    ;

assignment_expression
    : conditional_expression                                     { $$ = genN($1, "assignment_expression"); }
    | unary_expression assignment_operator assignment_expression { $$ = gen3N($1, $2, $3,"assignment_expression");}
    ;

assignment_operator
    : '='          { $$ = genT( "=","assignment_operator" ); }
    | MUL_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | DIV_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | MOD_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | ADD_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | SUB_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | LEFT_ASSIGN  { $$ = genT( $1, "assignment_operator" ); }
    | RIGHT_ASSIGN { $$ = genT( $1, "assignment_operator" ); }
    | AND_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | XOR_ASSIGN   { $$ = genT( $1, "assignment_operator" ); }
    | OR_ASSIGN    { $$ = genT( $1, "assignment_operator" ); }
    ;

expression
    : assignment_expression                 { $$ = genN($1, "expression"); }
    | expression ',' assignment_expression  { $$ = genNTN( $1, ",", $3, "expression"); }
    ;

constant_expression
    : conditional_expression   { $$ = genN($1, "constant_expression"); }
    ;

declaration
    : declaration_specifiers ';'                        { $$ = genNT( $1, ";", "declaration" );}
    | declaration_specifiers init_declarator_list ';'   { $$ = genNNT( $1, $2, ";", "declaration" ); }
    ;

declaration_specifiers
    : storage_class_specifier                       { $$ == genN( $1, "declaration_specifiers" ); }
    | storage_class_specifier declaration_specifiers{ $$ = gen2N( $1, $2, "declaration_specifiers" ); }
    | type_specifier                                { $$ = genN ( $1, "declaration_specifiers"); }
    | type_specifier declaration_specifiers         { $$ = gen2N( $1, $2, "declaration_specifiers"); }
    | type_qualifier                                { $$ = genN ( $1, "declaration_specifiers"); }
    | type_qualifier declaration_specifiers         { $$ = gen2N( $1, $2, "declaration_specifiers"); }
    ;

init_declarator_list
    : init_declarator                           { $$ = genN($1, "init_declarator_list"); }
    | init_declarator_list ',' init_declarator  { $$ = genNTN( $1, ",", $3, "init_declarator_list" ); }
    ;

init_declarator
    : declarator                 { $$ = genN($1, "init_declarator"); }
    | declarator '=' initializer { $$ = genNTN( $1, "=", $3, "init_declarator" ); } 
    ;

storage_class_specifier
    : TYPEDEF  { $$ = genT($1, "storage_class_specifier"); }
    | EXTERN   { $$ = genT($1, "storage_class_specifier"); }
    | STATIC   { $$ = genT($1, "storage_class_specifier"); }
    | AUTO     { $$ = genT($1, "storage_class_specifier"); }
    | REGISTER { $$ = genT($1, "storage_class_specifier"); }
    ;

type_specifier
	: VOID               { $$ = genT($1, "type_specifier"); }
	| CHAR               { $$ = genT($1, "type_specifier"); }
    | BOOL               { $$ = genT($1, "type_specifier"); }   
	| SHORT              { $$ = genT($1, "type_specifier"); }
	| INT                { $$ = genT($1, "type_specifier"); }
    | UINT               { $$ = genT($1, "type_specifier"); }
	| LONG               { $$ = genT($1, "type_specifier"); }
	| FLOAT              { $$ = genT($1, "type_specifier"); }
	| DOUBLE             { $$ = genT($1, "type_specifier"); }
	| SIGNED             { $$ = genT($1, "type_specifier"); }
	| UNSIGNED           { $$ = genT($1, "type_specifier"); }
	| struct_or_union_specifier  { $$ = genN($1, "type_specifier"); }
	| enum_specifier     { $$ = genN($1, "type_specifier"); }
	| TYPE_NAME          { $$ = genT($1, "type_specifier"); }
	;

struct_or_union_specifier
    : struct_or_union IDENTIFIER '{' struct_declaration_list '}'  { $$ = genNTTNT( $1, $2, "{", $4, "}", "struct_or_union_specifier" ); }
    | struct_or_union '{' struct_declaration_list '}'             { $$ = genNTNT( $1, "{", $3, "}", "struct_or_union_specifier"); }
    | struct_or_union IDENTIFIER                                  { $$ = genNT($1, $2, "struct_or_union_specifier"); }
    ;

struct_or_union
    : STRUCT  { $$ = genT($1, "struct_or_union"); }
    | UNION   { $$ = genT($1, "struct_or_union"); }
    ;

struct_declaration_list
    : struct_declaration                          { $$ = genN($1, "struct_declaration_list"); }
    | struct_declaration_list struct_declaration  { $$ = gen2N($1, $2, "struct_declarator_list"); }
    ;

struct_declaration
    : specifier_qualifier_list struct_declarator_list ';'  { $$ = genNNT( $1, $2, ";", "struct_declaration" ); }
    ;

specifier_qualifier_list
    : type_specifier specifier_qualifier_list  { $$ = gen2N($1, $2, "specifier_qualifier_list"); }
    | type_specifier                           { $$ = genN($1, "specifier_qualifier_list"); }
    | type_qualifier specifier_qualifier_list  { $$ = gen2N($1, $2, "specifier_qualifier_list"); }
    | type_qualifier                           { $$ = genN($1, "specifier_qualifier_list"); }
    ;

struct_declarator_list
    : struct_declarator                             { $$ = genN($1, "struct_declarator_list"); }
    | struct_declarator_list ',' struct_declarator  { $$ = genNTN($1, ",", $3, "struct_declarator_list"); }
    ;

struct_declarator
    : declarator                            { $$ = genN($1, "struct_declarator"); }
    | ':' constant_expression               { $$ = genTN(":", $2, "struct_declarator"); }
    | declarator ':' constant_expression    { $$ = genNTN( $1, ":", $3, "struct_declarator" ); }
    ;

enum_specifier
    : ENUM '{' enumerator_list '}'              { $$ = genTTNT( $1, "{", $3, "}", "enum_specifier"); }
    | ENUM IDENTIFIER '{' enumerator_list '}'   { $$ = genTTTNT( $1, $2, "{", $4 , "}", "enum_specifier" ); }
    | ENUM IDENTIFIER                           { $$ = genTT( $1, $2, "enum_specifier" ); }
    ;   

enumerator_list
    : enumerator                        { $$ = genN($1, "enumerator_list"); }
    | enumerator_list ',' enumerator    { $$ = genNTN($1, ",", $3, "enumerator_list"); }
    ;

enumerator
    : IDENTIFIER                            { $$ = genT($1, "enumerator"); }
    | IDENTIFIER '=' constant_expression    { $$ = genTTN( $1, "=", $3, "enumerator"); }
    ;

type_qualifier
    : CONST     { $$ = genT($1, "type_qualifier"); }
    | VOLATILE  { $$ = genT($1, "type_qualifier"); }
    ;

declarator
    : pointer direct_declarator     { $$ = gen2N($1, $2, "declarator"); }
    | direct_declarator             { $$ = genN($1, "declarator"); }
    ;

direct_declarator
    : IDENTIFIER                                    { $$ = genT( $1, "direct_declarator" ); } 
    | '(' declarator ')'                            { $$ = genTNT( "(", $2, ")", "direct_declarator" ); }
    | direct_declarator '[' constant_expression ']' { $$ = genNTNT( $1, "[", $3, "]", "direct_declarator"); }
    | direct_declarator '[' ']'                     { $$ = genNTT( $1, "[", "]", "direct_declarator" ); }
    | direct_declarator '(' parameter_type_list ')' { $$ = genNTNT( $1, "(", $3, ")", "direct_declarator" ); }
    | direct_declarator '(' identifier_list ')'     { $$ = genNTNT( $1, "(", $3, ")", "direct_declarator" ); }
    | direct_declarator '(' ')'                     { $$ = genNTT( $1,"(", ")", "direct_declarator" ); }
    ;

pointer
    : '*'                               { $$ = genT("*pointer","pointer");       }
    | '*' type_qualifier_list           { $$ = genTN("*pointer", $2, "pointer"); }
    | '*' pointer                       { $$ = genTN("*pointer", $2, "pointer"); }
    | '*' type_qualifier_list pointer   { $$ = genTNN("*pointer", $2, $3, "pointer"); }
    ;

type_qualifier_list
    : type_qualifier                     { $$ = genN($1, "type_qualifier_list"); } 
    | type_qualifier_list type_qualifier { $$ = gen2N( $1, $2, "type_qualifier_list" ); }
    ;


parameter_type_list
    : parameter_list                { $$ = genN( $1, "parameter_type_list"); }
    | parameter_list ',' ELLIPSIS   { $$ = genNTT( $1, ",", $3, "parameter_type_list" ); }
    ;

parameter_list
    : parameter_declaration                    { $$ = genN($1, "parameter_list"); }
    | parameter_list ',' parameter_declaration { $$ = genNTN( $1, ",", $3, "parameter_list" ); }
    ;

parameter_declaration
    : declaration_specifiers declarator  { $$ = gen2N( $1, $2, "parameter_declaration" ); }
    | declaration_specifiers abstract_declarator  { $$ = gen2N( $1, $2, "parameter_declaration" ); }
    | declaration_specifiers  { $$ = genN( $1, "parameter_declaration" ); }
    ;

identifier_list
    : IDENTIFIER                     { $$ = genT( $1, "identifier_list" ); }
    | identifier_list ',' IDENTIFIER { $$ = genNTT( $1, ",", $3, "identifier_list" ); }
    ;

type_name
    : specifier_qualifier_list  { $$ = genN( $1, "type_name" ); }
    | specifier_qualifier_list abstract_declarator  { $$ = gen2N( $1, $2, "type_name"); }
    ;

abstract_declarator
    : pointer                           { $$ = genN( $1, "abstract_declarator" ); }
    | direct_abstract_declarator        { $$ = genN( $1, "abstract_declarator" ); }
    | pointer direct_abstract_declarator{ $$ = gen2N( $1, $2, "abstract_declarator" ); }
    ;

direct_abstract_declarator
    : '(' abstract_declarator ')'           { $$ = genTNT( "(", $2 , ")", "direct_abstract_declarator" ); }
    | '[' ']'                               { $$ = genTT(  "[", "]", "direct_abstract_declarator" ); }
    | '[' constant_expression ']'           { $$ = genTNT( "{", $2 , "}", "direct_abstract_declarator" ); }
    | direct_abstract_declarator '[' ']'    { $$ = genNTT( $1, "[" , "]", "direct_abstract_declarator" ); }
    | direct_abstract_declarator '[' constant_expression ']'  { $$ = genNTNT( $1, "[", $3, "]", "direct_abstract_declarator" ); }

    | '(' ')'                           { $$ = genTT(  "(", ")", "direct_abstract_declarator" ); }

    | '(' parameter_type_list ')'           { $$ = genTNT( "(", $2, ")", "direct_abstract_declarator"); }
    | direct_abstract_declarator '(' ')'    { $$ = genNTT( $1, "(", ")", "direct_abstract_declarator" ); }
    | direct_abstract_declarator '(' parameter_type_list ')'    { $$ = genNTNT( $1, "(", $3, ")", "direct_abstract_declarator" ); }
    ;

initializer
    : assignment_expression         { $$ = genN( $1, "initializer" ); }
    | '{' initializer_list '}'      { $$ = genTNT("{", $2, "}", "initializer"); } 
    | '{' initializer_list ',' '}'  {$$ = genTNTT("{", $2, ",", "}", "initializer");}
    ;

initializer_list
    : initializer                       { $$ = genN( $1, "initializer_list" ); }
    | initializer_list ',' initializer  { $$ = genNTN($1, ",", $3, "initializer_list"); }
    ;

statement
    : labeled_statement     { $$ = genN($1, "statement"); }
    | compound_statement    { $$ = genN($1, "statement"); }
    | expression_statement  { $$ = genN($1, "statement"); }
    | selection_statement   { $$ = genN($1, "statement"); }
    | iteration_statement   { $$ = genN($1, "statement"); }
    | jump_statement        { $$ = genN($1, "statement"); }
    ;

labeled_statement
    : IDENTIFIER ':' statement              { $$ = genTTN($1, ":", $3, "labeled_statement"); }
    | CASE constant_expression ':' statement{ $$ = genTNTN($1, $2, ":", $4, "labeled_statement"); }
    | DEFAULT ':' statement                 { $$ = genTTN($1, ":", $3, "labeled_statement"); }
    ;

compound_statement
    : '{' '}'                                 {$$ = genTT ("{", "}", "compound_statement" );}
    | '{' statement_list '}'                  {$$ = genTNT("{", $2, "}","compound_statement");}
    | '{' declaration_list '}'                {$$ = genTNT("{", $2, "}","compound_statement");}
    | '{' declaration_list statement_list '}' {$$ = genTNNT("{", $2, $3, "}","compound_statement");}
    ;

declaration_list
    : declaration                   { $$ = genN( $1, "declaration_list" ); }
    | declaration_list declaration  { $$ = gen2N( $1, $2, "declaration_list" ); }
	;

statement_list
    : statement                 { $$ = genN($1, "statement"); }
    | statement_list statement  { $$ = gen2N($1, $2, "statement_list"); }
    ;

expression_statement
    : ';'             { $$ = genT(";", "expression"); }
    | expression ';'  { $$ = genNT($1, ";", "expression_statement"); }
    ;

selection_statement
	: IF '(' expression ')' statement  %prec LOWER_THAN_ELSE  { $$ = genTTNTN( $1, "(", $3, ")", $5, "selection_statement" ); }
	| IF '(' expression ')' statement ELSE statement    { $$ = genTTNTNTN( $1, "(", $3, ")", $5, $6, $7, "selection_statement"); }
	| SWITCH '(' expression ')' statement               { $$ = genTTNTN( $1, "(", $3, ")", $5, "selection_statement" ); }
	;

iteration_statement
    : WHILE '(' expression ')' statement                { $$ = genTTNTN( $1, "(", $3, ")", $5, "iteration_statement" ); }       
    | DO statement WHILE '(' expression ')' ';'         { $$ = genTNTTNTT( $1, $2, $3, "(", $5, ")", ";", "iteration_statement" ); }
    | FOR '(' expression_statement expression_statement ')' statement     {  $$ = genTTNNTN($1, "(", $3, $4, ")", $6, "iteration_statement");}
    | FOR '(' expression_statement expression_statement expression ')' statement   { $$ = genTTNNNTN($1, "(", $3, $4, $5, ")", $7, "iteration_statement"); }
    ;

jump_statement
    : GOTO IDENTIFIER ';'      { $$ = gen3T($1, $2, ";", "jump_statement"); }
    | CONTINUE ';'             { $$ = genTT($1, ";", "jump_statement"); }
    | BREAK ';'                { $$ = genTT($1, ";", "jump_statement"); }
    | RETURN ';'               { $$ = genTT($1, ";", "jump_statement"); }
    | RETURN expression ';'    { $$ = genTNT($1, $2, ";", "jump_statement"); }
    ;

translation_unit
    : external_declaration                  { $$ = genN($1, "translation_unit");} 
    | translation_unit external_declaration { $$ = gen2N( $1, $2, "translation_unit" ); }
    | preprocessing_file                    { $$ = genN ($1, "translation_unit"); }
    | translation_unit preprocessing_file   { $$ = gen2N( $1, $2, "translation_unit" ); }
    | error                                 { eatNewLine(); yyclearin; }
    ;

external_declaration
    : function_definition { $$ = genN($1, "external_declaration");}
    | declaration         { $$ = genN($1, "external_declaration"); }
    ;

function_definition
    : declaration_specifiers declarator declaration_list compound_statement { $$ = gen4N($1, $2, $3, $4, "function_definition"); }
    | declaration_specifiers declarator compound_statement  { $$ = gen3N($1, $2, $3, "function_definition"); }
    | declarator declaration_list compound_statement        { $$ = gen3N($1, $2, $3, "function_definition"); }
    | declarator compound_statement                         { $$ = gen2N($1, $2,  "function_definition");}
    ;

%%
#include <stdio.h>

extern char yytext[];
extern int column;
extern char* line;
extern int curLineBufLen;
extern int initLineBufLen;
extern int lineno;
extern int pre_check;
extern void openFile();

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void pre_checkSetZero(){
    pre_check = 0;
}

void yyerror(char *s)
{
	fflush(stderr);
    printf(ANSI_COLOR_RED "Parse Error: Line: %d Column: %d\n" ANSI_COLOR_RESET, lineno, column);
    printf("%s\n%*s\n",line, column, "^");
}

int main(void){
    FILE *f = fopen("graph.dot", "a");
    fprintf(f,"digraph G{");
    fclose(f);
    /*openFile();*/
    yyparse();
    return 0;
}
