
D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*

%{
#include <stdio.h>
#include <string.h>
#include "y.tab.h"

/*#define YYSTYPE int*/
/*extern int YYSTYPE;*/
/*void count();*/
int pre_check = 0;
void count(char*);
void comment();
int buffer_length = 10; //For scanning preprocessor directives without an #end<> tag
extern void eatNewLine();
%}

%%

"/*"			{ comment(); }
"#"[ ]*"if"         { pre_check =1; count(""); return(PRE_IF);}
"#"[ ]*"ifdef"      { count("#"); return(PRE_IFDEF);}
"#"[ ]*"ifndef"     { count("#"); return(PRE_IFNDEF);}
"#"[ ]*"elif"       { count("#"); return(PRE_ELIF);}
"#"[ ]*"else"       { count("#"); return(PRE_ELSE);}
"#"[ ]*"endif"      { count("#"); return(PRE_ENDIF);}
"#"[ ]*"include"    { count("#"); return(PRE_INCLUDE);} 
"#"[  ]*"define"    { count("#"); return(PRE_DEFINE);}
"#"[ ]*"undef"      { count("#"); return(PRE_UNDEF);}
"#"[ ]*"line"       { count("#"); return(PRE_LINE);}
"#"[  ]*"error"     { count("#"); return(PRE_ERROR);} 
"#"[ ]*"pragma"     { count("#"); return(PRE_PRAGMA);}


"auto"			{ count(""); return(AUTO); }
"break"			{ count(""); return(BREAK); }
"bool"          { count(""); return(BOOL); }
"case"			{ count(""); return(CASE); }
"char"			{ count(""); return(CHAR); }
"const"			{ count(""); return(CONST); }
"continue"		{ count(""); return(CONTINUE); }
"default"		{ count(""); return(DEFAULT); }
"do"			{ count(""); return(DO); }
"double"		{ count(""); return(DOUBLE); }
"else"			{ count(""); return(ELSE); }
"enum"			{ count(""); return(ENUM); }
"extern"		{ count(""); return(EXTERN); }
"float"			{ count(""); return(FLOAT); }
"for"			{ count(""); return(FOR); }
"goto"			{ count(""); return(GOTO); }
"if"			{ count(""); return(IF); }
"int"			{ count(""); return(INT); }
"uint"          { count(""); return(UINT); }
"long"			{ count(""); return(LONG); }
"register"		{ count(""); return(REGISTER); }
"return"		{ count(""); return(RETURN); }
"short"			{ count(""); return(SHORT); }
"signed"		{ count(""); return(SIGNED); }
"sizeof"		{ count(""); return(SIZEOF); }
"static"		{ count(""); return(STATIC); }
"struct"		{ count(""); return(STRUCT); }
"switch"		{ count(""); return(SWITCH); }
"typedef"		{ count(""); return(TYPEDEF); }
"union"			{ count(""); return(UNION); }
"unsigned"		{ count(""); return(UNSIGNED); }
"void"			{ count(""); return(VOID); }
"volatile"		{ count(""); return(VOLATILE); }
"while"			{ count(""); return(WHILE); }

{L}({L}|{D})*		{ count(""); return(IDENTIFIER);/*return(check_type());*/ }

0[xX]{H}+{IS}?		{ count(""); return(CONSTANT); }
0{D}+{IS}?		{ count(""); return(CONSTANT); }
{D}+{IS}?		{   count("");
                    /*yylval = atoi(yytext);*/
                    return(CONSTANT); 
                }
L?'(\\.|[^\\'])+'	{ count(""); return(CONSTANT); }

{D}+{E}{FS}?		{ count(""); return(CONSTANT); }
{D}*"."{D}+({E})?{FS}?	{ count(""); return(CONSTANT); }
{D}+"."{D}*({E})?{FS}?	{ count(""); return(CONSTANT); }

L?\"(\\.|[^\\"])*\"	{ count("STRING_LITERAL");return(STRING_LITERAL); }

"..."			{ count(""); return(ELLIPSIS); }
">>="			{ count(""); return(RIGHT_ASSIGN); }
"<<="			{ count(""); return(LEFT_ASSIGN); }
"+="			{ count(""); return(ADD_ASSIGN); }
"-="			{ count(""); return(SUB_ASSIGN); }
"*="			{ count(""); return(MUL_ASSIGN); }
"/="			{ count(""); return(DIV_ASSIGN); }
"%="			{ count(""); return(MOD_ASSIGN); }
"&="			{ count(""); return(AND_ASSIGN); }
"^="			{ count(""); return(XOR_ASSIGN); }
"|="			{ count(""); return(OR_ASSIGN); }
">>"			{ count(""); return(RIGHT_OP); }
"<<"			{ count(""); return(LEFT_OP); }
"++"			{ count(""); return(INC_OP); }
"--"			{ count(""); return(DEC_OP); }
"->"			{ count(""); return(PTR_OP); }
"&&"			{ count(""); return(AND_OP); }
"||"			{ count(""); return(OR_OP); }
"<="			{ count(""); return(LE_OP); }
">="			{ count(""); return(GE_OP); }
"=="			{ count(""); return(EQ_OP); }
"!="			{ count(""); return(NE_OP); }
";"			{ count(""); return(';'); }
("{"|"<%")		{ count(""); return('{'); }
("}"|"%>")		{ count(""); return('}'); }
","			{ count(""); return(','); }
":"			{ count(""); return(':'); }
"="			{ count(""); return('='); }
"("			{ count(""); return('('); }
")"			{ count(""); return(')'); }
("["|"<:")		{ count(""); return('['); }
("]"|":>")		{ count(""); return(']'); }
"."			{ count(""); return('.'); }
"&"			{ count(""); return('&'); }
"!"			{ count(""); return('!'); }
"~"			{ count(""); return('~'); }
"-"			{ count(""); return('-'); }
"+"			{ count(""); return('+'); }
"*"			{ count(""); return('*'); }
"/"			{ count(""); return('/'); }
"%"			{ count(""); return('%'); }
"<"			{ count(""); return('<'); }
">"			{ count(""); return('>'); }
"^"			{ count(""); return('^'); }
"|"			{ count(""); return('|'); }
"?"			{ count(""); return('?'); }

[ \t\v\f]		{ count(""); }
[\n]            { count(""); 
                    if(pre_check != 0){
                        /*pre_check = 0;*/
                        return(NEWLINE);
                    }
                 }
.			{ /* ignore bad characters */ }

%%

int yywrap()
{
	return 1;
}



int column = 0;
int lineno = 1;
int initLineBufLen = 100;
int curLineBufLen = 0;
char* line = "";

char* prevline = "";
int prevcolumn = 0;
int prevlineno = 1;

void count(char* token)
{
	int i;
    yylval.code = strdup(yytext);
    
    int tmplen = strlen(line) + strlen(yytext);
    if(tmplen > curLineBufLen){
        char*tmp = strdup(line);
        curLineBufLen = (tmplen/initLineBufLen + 1) * initLineBufLen;
        line = malloc(sizeof(char) * (curLineBufLen));
        sprintf(line,"%s%s", line, yytext);
    }
    else{
        sprintf(line,"%s%s",line,yytext);
    }
    
        
    if(strcmp(token,"STRING_LITERAL") == 0){
        int lentmp = strlen(yytext) + 3;
        char* tmp = malloc(sizeof(char) * lentmp);
        char* tmp2 = strdup(yytext);
        strcpy(tmp+1,tmp2);
        tmp[0] = '\\';
        tmp[lentmp - 3] = '\\';
        tmp[lentmp - 2] = '\"';
        tmp[lentmp-1] = '\0';
        yylval.code = strdup(tmp);
    }
    else if(strcmp(token,"#") == 0){
        pre_check = 1;
    }

	for (i = 0; yytext[i] != '\0'; i++){
        if (yytext[i] == '\n'){
            prevline = strdup(line);
            prevlineno = lineno;
            prevcolumn = column;

            lineno++;
			column = 0;
            free(line);
            line = malloc(sizeof(char)*initLineBufLen);
            line[0] = '\0';
        }
		else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;
    }
    /*ECHO;*/
}


int check_type()
{
	return(IDENTIFIER);
}

void comment()
{
	char c, c1;

loop:
	while ((c = input()) != '*' && c != 0)
	{	/*putchar(c);*/ }

	if ((c1 = input()) != '/' && c != 0)
	{
		/*unput(c1);*/
		goto loop;
	}

	if (c != 0)
    {/*	putchar(c1);*/ }
}

void eatNewLine(){
    int c;
    while((c = getchar()) != EOF && c != '\n');
}
